<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'siegeislife_blog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|R,>&[A]KoMr]Rt<c@ZFtIyKDJqY)XL_bNL?$#fhO#@Qwy/~*>?~9wG6Alm[72F&');
define('SECURE_AUTH_KEY',  'eK%x8,`]tLvpfX@F3:?AjNprIN>Tx&H<xg0/_*1AW0,P:i7O!!*?D$a}R^-wGPOD');
define('LOGGED_IN_KEY',    'bV=nAAEnxIYu`MnP=18w:h?_mOuYmBK.EE.#_2YMhJ,uEoi<#YU7,,|_e6 P0vJ3');
define('NONCE_KEY',        ',o<q2o$ ZAINr)^4CP[Z:Mhg$J/X0h7A?)^IbEH>2:B&@ulBv z?Kvf6e{52y32=');
define('AUTH_SALT',        '?85`a8Q Q4[<($mx:[T#k_+B~^0y&mV?85B*aN+Ek!VkbRmA9]!D*oFB?9@YlPUn');
define('SECURE_AUTH_SALT', 'J9_#&OZ=2hH_MksSP(M{)iFk&~?HIR!+w%:R)=1;_Qx(c*^jtA@(7[5;fuuwoqxX');
define('LOGGED_IN_SALT',   'mmt+5PoH( T?B_O-n:E|/:RMGj#H7d(hq,>.C*I:;oAw.K<l?hp-oO7r7Lt=Zf?H');
define('NONCE_SALT',       'N%6NVVy !PFU/|f-*F[y8/5@Fskz|;p)TJ9p#Zv=^=8B:x*=7-7Pp >o>dY=,;qG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
