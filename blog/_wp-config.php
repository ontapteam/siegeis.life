<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'blog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'D{oV5cfR(kSL.]WFzW{WwT}A?8@x-b(g:iucY++ts0;<:3dM]ErE35~9Iv3xX#<~');
define('SECURE_AUTH_KEY',  ';,F1*9M>dqT@`>7}3Zp=x3P1,gEo~&{R3XSE4vxC*0)FFCnGkWSY5Q&`LB+(%3n:');
define('LOGGED_IN_KEY',    'vbUN/#D~.&E2N{sGs}2Da5AsB@yDHs(1CDxIZu.U5o2rN<G`h>u@k}R]cpEl=uOv');
define('NONCE_KEY',        'R/kJubnTScpfC^?OsTL<G#Y6HF~*^RtNCebAH&Eu>*Js,n4gP;@xe4]kK,FX(eoU');
define('AUTH_SALT',        'Rs 8q-*OhJ-e4 *`WtK)MN:ClU6}73TM%Lh->Ol$Rvii2/G.uf*6I{JwUTxp^_b1');
define('SECURE_AUTH_SALT', 'q1RD=f|QnW}2X)J5+_!FR<.uAW_T5eXDdE+-[G]?PL*QvZo+ `mYp5+6vU%-S39@');
define('LOGGED_IN_SALT',   '2%0uwXP,`4@J.}ZftLZnJ&ZAlRGAgh?h%V2h(Im]r,Kt:N~scw-7L*5gSL8&$Nwn');
define('NONCE_SALT',       'zX;Z#}eUF,|mjJ_KAJ+ b)LHL:~7UFB_p,X=XGQA#_DUBnW.<hw/X-|.QOe.PP>d');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
