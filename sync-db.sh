#!/bin/bash
scp root@siegeis.life:/var/www/siege/backup/siegeislife_latest.sql.gz .
gunzip siegeislife_latest.sql.gz
pv -nfi 0.25 ./siegeislife_latest.sql | sed 's/ROW_FORMAT=FIXED//g' | sed 's/DEFINER=[^*]*\*/\*/g' | mysql -h siege-mysql.local.com -umagento -pmagento magento
