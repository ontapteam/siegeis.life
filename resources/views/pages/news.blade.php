@extends('layouts.app')

@section('content')
    <div class="container" id="blog-posts">
        <div class="row">
            <div class="col s12 m4 l11 push-l1">
                <h1 class="main-title white-text">News</h1>
                @include('partials.header.breadcrumbs')
            </div>
            @include('partials.blog.sidebar')
            @include('partials.blog.list')
        </div>
    </div>
@endsection