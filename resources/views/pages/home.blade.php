@extends('layouts.app')

@section('content')

    @if (Session::has('message'))
        <div class="row">
            <div class="col s12">
                <div class="card {{ \App\Colours::getPrimaryColour() }}">
                    <div class="card-content white-text">
                        <div class="col parent s12"><span class="card-title black-text">Oops, something went wrong...</span></div>
                        {{ Session::get('message') }}
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col s12 m6 parent">
            @if (Auth::guest())
                <div class="col s12">
                    @include ('cards.basic', ['title' => "Introducing", 'blade' => 'home.welcome', 'additional_data' => ""] )
                </div>
                <div class="col s12 m12 l6">
                    @include ('cards.basic', ['title' => "Register", 'blade' => 'home.register', 'additional_data' => ""] )
                </div>
                <div class="col s12 m12 l6">
                    @include ('cards.basic', ['title' => "Login", 'blade' => 'home.login', 'additional_data' => ""] )
                </div>
            @else
                <div class="col s12">
                    @include ('cards.basic', ['title' => Auth::user()->name . ", Welcome to", 'blade' => 'home.welcome', 'additional_data' => ""] )
                </div>
                <div class="col s12 m12 l6">
                    @include ('cards.basic', ['title' => "Stats", 'blade' => 'home.viewstats', 'additional_data' => ""] )
                </div>
                <div class="col s12 m12 l6">
                    @include ('cards.basic', ['title' => "Logout", 'blade' => 'home.logout', 'additional_data' => ""] )
                </div>
            @endif
            <div class="col s12" id="blog-posts">
                @include ('cards.featured', ['title' => "Featured", 'blade' => 'home.featured', 'additional_data' => ""] )
            </div>
        </div>

    </div>

@endsection
