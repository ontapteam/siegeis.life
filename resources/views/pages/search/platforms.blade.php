@extends('layouts.app')

@section('content')

    <div class="icon-container">
        <search-platform :username="'{{ $username }}'" :platform="'xone'" :img="'/images/xone.png'"></search-platform>
        <search-platform :username="'{{ $username }}'" :platform="'ps4'" :img="'/images/ps4.png'"></search-platform>
        <search-platform :username="'{{ $username }}'" :platform="'uplay'" :img="'/images/pc.png'"></search-platform>
    </div>

@endsection
