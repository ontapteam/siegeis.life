<form method="POST" action="/account/{{ auth()->user()->id }}">
    {{ csrf_field() }}
    <input name="_method" type="hidden" value="PATCH">
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" value="{{ auth()->user()->name }}" class="form-control">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" value="{{ auth()->user()->email }}" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>