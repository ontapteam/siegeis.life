@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col s12 m10 offset-m1 l8 offset-l2 parent">
            <div class="col s12">
                @include ('cards.account')
            </div>
        </div>
    </div>

@endsection