@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="season-nav">

            <player colour="{{ \App\Colours::getSecondaryColour() }}" username="{{ $player->username }}" platform="{{ $player->platform }}"></player>

    </div>

@endsection