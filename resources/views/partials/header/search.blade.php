<form id="{{ $id }}" action="/search" method="POST">

    <div class="row" id="search-row">

        {{ csrf_field() }}

        <div class="input-field col s9 m10 l8">

            <?php if(isset($player)):
            if (isset($player->username)) {
                $username = $player->username;
            }
            ?>
            <input id="player-name" type="text" name="player" aria-label="player" value="{{ $username }}" placeholder="Enter player name"/>
            <?php else: ?>
            <input id="player-name" type="text" name="player" aria-label="player" placeholder="Enter player name"/>
            <?php endif; ?>

        </div>

        <div class="input-field col s3 m2 l2">
            <select id="platform-selector" name="platform">
                <?php
                if (isset($player->platform)):
                $platform = $player->platform;
                ?>

                <?php if($platform == "xone"): ?>
                <option value="xone">XBOX</option>
                <option value="ps4">PS4</option>
                <option value="uplay" selected>PC</option>
                <?php elseif($platform == "ps4"): ?>
                <option value="xone">XBOX</option>
                <option value="ps4" selected>PS4</option>
                <option value="uplay">PC</option>
                <?php elseif($platform == "uplay"): ?>
                <option value="xone">XBOX</option>
                <option value="ps4">PS4</option>
                <option value="uplay" selected>PC</option>
                <?php endif; ?>

                <?php else: ?>
                <option value="xone">XBOX</option>
                <option value="ps4">PS4</option>
                <option value="uplay" selected>PC</option>
                <?php endif; ?>
            </select>
        </div>

        <div class="input-field col s12 l2">
            <button type="submit" class="search-submit-btn waves-effect waves-light btn"><i class="fa fa-search" aria-hidden="true"></i>Search</button>
        </div>

    </div>

</form>
