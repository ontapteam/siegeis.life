@foreach($menu->menu as $item)
    <li class="{{ $item[2] }}"><a href="{{ $item[1] }}" alt="{{ $item[0] }}"><i class="fa fa-{{ $item[3] }}" aria-hidden="true"></i><span>{{ $item[0] }}</span></a></li>
@endforeach

@if (Auth::guest())

@else
    <li class="link--account"><a href="/account/{{ Auth::user()->id  }}/edit"><i class="fa fa-user" aria-hidden="true"></i><span>Account</span></a></li>
    <li class="link--logout">
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out" aria-hidden="true"></i><span>Logout</span>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

    </li>
@endif