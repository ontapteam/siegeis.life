@if (Auth::guest())
    <li class="link--login"><a href="/login"><i class="fa fa-users" aria-hidden="true"></i><span class="hide-on-small-and-down">Login</span></a></li>
    <li class="link--register"><a href="/register"><i class="fa fa-pencil" aria-hidden="true"></i><span class="hide-on-small-and-down">Register</span></a></li>
@else
    <li class="link--account"><a href="/account/{{ Auth::user()->id  }}/edit"><i class="fa fa-user" aria-hidden="true"></i><span>Account</span></a></li>
    <li class="link--logout">
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out" aria-hidden="true"></i><span>Logout</span>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

    </li>
@endif