<div class="col hide-on-mobile l3 push-l1" id="sidebar">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="card {{ \App\Colours::getSecondaryColour() }}">
                <div class="card-content white-text">
                    <div class="list">
                        <h6>Categories</h6>
                        <ul>
                            <li><a href="/news" class="{{ \App\Menu::getActive('news') }}">News</a></li>
                            @foreach($cats as $cat)
                                <li><a href="/news/category/{{ strtolower($cat) }}" class="{{ \App\Menu::getActive($cat) }}">{{ $cat }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="list">
                        <h6>Social</h6>
                        <div class="facebook-feed">
                            <div class="fb-page" style="width:100%;" data-href="https://www.facebook.com/r6siegeislife" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                <blockquote cite="https://www.facebook.com/r6siegeislife" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/r6siegeislife">Siege is Life</a></blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>