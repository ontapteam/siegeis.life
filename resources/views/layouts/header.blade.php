<?php $menu = new \App\Menu; ?>

<nav class="{{ \App\Colours::getSecondaryColour() }}">
    <div class="nav-wrapper">
        <a href="/" id="brand-logo" class="brand-logo" aria-label="Go back to Home Page" title="Siege is Life"> Siege is Life.</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse" aria-label="Open Mobile Menu" title="Mobile Menu"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul id="main-menu" class="right hide-on-med-and-down">
            @include ('partials.header.nav')
        </ul>
        <ul id="account-menu" class="right hide-on-large-only">
            @include ('partials.header.account-nav')
        </ul>
        <div class="search right hide-on-med-and-down">
            @include ('partials.header.search', ['id' => 'search-player'])
        </div>
        <ul class="side-nav" id="mobile-demo">
            @include ('partials.header.nav')
        </ul>

        <div class="row hide-on-large-only" style="margin:0;">
            <div class="col s12">
                @include ('partials.header.search', ['id' => 'mobile-search'])
            </div>
        </div>
    </div>
</nav>
