<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="/manifest.json">
    <meta name="theme-color" content="#2196F3">
    <link rel="icon" href="/images/siege-symbol-favicon.png" sizes="16x16" type="image/png">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};

        async function getJsonInParallel(urls) {
            // fetch all the URLs in parallel
            setTimeout(function () {

                const textPromises = urls.map(async url => {
                    const response = await fetch(url, {mode: "no-cors", timeout: 5000});
                    return response.text();
                });

                // log them in sequence
                for (const textPromise of textPromises) {
                    //console.log(await textPromise);
                }

            }, 500);

        }

        async function getJson(urls) {

            console.log(urls);
            for (const url of urls) {
                const response = await fetch(url, {mode: "no-cors", timeout: 5000});
                console.log(await response.json());
            }

        }

        function getvals(url) {
            return fetch(url,
                {
                    method: "GET",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                })
                .then((response) => response.json())
                .then((responseData) => {
                    //console.warn(responseData);
                    return responseData;
                })
        }


    </script>

    <style>
        body {
            background-color : #1976D2;
            color            : white;
            font-family      : Oswald, sans-serif;
            font-size        : 14px;
            line-height      : 1.1;
        }

        .preloader-wrapper,
        .nav-wrapper,
        #app,
        footer {
            display : none;
        }

        footer {
            text-align : center;
        }

        a {
            color : #00A8EF;
        }

    </style>

</head>

<body bgcolor="#1976D2">

<?php /*
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1399799733573619";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
*/ ?>

@include ('layouts.header')
<main id="app">
    <div class="container">
        @yield('content')
    </div>
</main>
@include ('layouts.footer')

<noscript id="deferred-styles">
    <link rel="stylesheet" type="text/css" href="/css/app.css"/>
</noscript>

<script>

    var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
    else window.addEventListener('load', loadDeferredStyles);

    function loadCSS(href, before, media) {
        'use strict';

        var ss = window.document.createElement("link");
        var ref = before || window.document.getElementsByTagName("script")[0];
        var sheets = window.document.styleSheets;
        ss.rel = "stylesheet";
        ss.href = href;
        ss.media = "only x";
        ref.parentNode.insertBefore(ss, ref);
        function toggleMedia() {
            var defined;
            for (var i = 0; i < sheets.length; i++) {
                if (sheets[i].href && sheets[i].href.indexOf(href) > -1) {
                    defined = true;
                }
            }
            if (defined) {
                ss.media = media || "all";
            }
            else {
                setTimeout(toggleMedia);
            }
        }

        toggleMedia();
        return ss;
    }

    function loadScript(src) {
        'use strict';

        return new Promise(function (resolve, reject) {
            var s;
            s = document.createElement('script');
            s.src = src;
            s.onload = resolve;
            s.onerror = reject;
            document.head.appendChild(s);
        });
    }

    function loadImg(url) {
        'use strict';

        return new Promise(function (resolve, reject) {
            var request = new XMLHttpRequest();
            request.open('GET', url);
            request.responseType = 'blob';

            request.onload = function () {
                if (request.status == 200) {
                    resolve(request.response);
                } else {
                    reject(Error('Image didn\'t load successfully; error code:' + request.statusText));
                }
            };

            request.onerror = function () {
                reject(Error('There was a network error.'));
            };

            request.send();
        });
    }

    (function () {
        'use strict';

        if ('serviceWorker' in navigator) {
            window.addEventListener('load', function () {
                navigator.serviceWorker.register('{{ asset('service-worker.js') }}', {insecure: false})
                    .then(function (registration) {
                        // Registration was successful
                        console.log('ServiceWorker registration successful with scope: ', registration.scope);

                        var body = document.getElementById('brand-logo');
                        var myImage = new Image();

                        //loadCSS('/css/app.css');
                        loadCSS('https://fonts.googleapis.com/css?family=Miriam+Libre:400,700|Roboto+Condensed:300,400,700|Open+Sans|Oswald');
                        loadScript('/js/app.js');

                        loadImg('/images/siege-symbol.png').then(function (response) {
                            var imageURL = window.URL.createObjectURL(response);
                            myImage.src = imageURL;
                            myImage.className = "r6-symbol";
                            body.prepend(myImage);
                        }, function (Error) {
                            //console.log(Error);
                        });

                    }, function (err) {
                        // registration failed :(
                        // console.log('ServiceWorker registration failed: ', err);
                    });
            });
        } else {
            //loadCSS('/css/app.css');
            loadCSS('https://fonts.googleapis.com/css?family=Miriam+Libre:400,700|Roboto+Condensed:300,400,700|Open+Sans|Oswald');
            loadScript('/js/app.js');

            var body = document.getElementById('brand-logo');
            var myImage = new Image();

            loadImg('/images/siege-symbol.png').then(function (response) {
                var imageURL = window.URL.createObjectURL(response);
                myImage.src = imageURL;
                myImage.className = "r6-symbol";
                body.appendChild(myImage);
            }, function (Error) {
                //console.log(Error);
            });
        }

    })();

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '/js/analytics.js', 'ga');

    ga('create', 'UA-10811266-16', 'auto');
    ga('send', 'pageview');

</script>


</body>

</html>
