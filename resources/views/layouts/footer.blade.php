<?php

/*

<li><a href="/account/{{ Auth::user()->id  }}/edit">Edit Account</a></li>
        <li>
            <a href="{{ route('logout') }}"
               onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">
                   Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
{{ csrf_field() }}
</form>

</li>

*/

?>

<footer class="page-footer grey darken-4">
    <div class="footer-copyright">
        <div class="container">
            <div class="copyright">&copy; Siege is Life 2017 - For the W.</div>
        </div>
    </div>
</footer>