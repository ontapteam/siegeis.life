<div class="card {{ \App\Colours::getSecondaryColour() }}">
    <div class="card-content white-text">
        <div class="col parent s12"><span class="card-title {{ \App\Colours::getHeadingTextColor() }}">Account</span></div>
        @include ('pages.account.form')
    </div>
</div>