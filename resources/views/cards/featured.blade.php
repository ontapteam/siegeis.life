<?php if(isset($featured)): ?>
<div class="card {{ \App\Colours::getSecondaryColour() }} white-text">
    @if($featured->thumbnail)
        <div class="card-image">
            <div class="image-container" style="background-size:cover; background-position:top left; background-image:url('<?php echo $featured->thumbnail->size('full'); ?>');">
                <picture style="visibility: hidden;">
                    <source srcset="http://placehold.it/800x500" media="(max-width: 480px)">
                    <source srcset="http://placehold.it/800x400" media="(min-width: 481px) and (max-width: 768px)">
                    <img srcset="http://placehold.it/800x350" alt="My default image">
                </picture>
            </div>
            <h2 class="card-title">{{ $featured->post_title }}</h2>
        </div>
    @endif
    <div class="card-content">
        @if(!$featured->thumbnail)
            <h2 class="card-title no-image">{{ $featured->post_title }}</h2>
        @endif
        <p>{{ \App\WordpressPost::getExcerpt($featured->content) }}</p>
        <div class="meta">
            <h6 class="post-date">Author : <span class="{{ \App\Colours::getHeadingTextColor() }}">S.I.L. Team</span></h6>
            <h6 class="post-date">Date Published: <span class="{{ \App\Colours::getHeadingTextColor() }}">{{ \Carbon\Carbon::parse($featured->post_date)->format('D, d/m/y, H:i:s') }}</span></h6>
        </div>
    </div>
    <div class="card-action">
        <a class="{{ \App\Colours::getHeadingTextColor() }}" href="/news/{{ $featured->post_name }}">Read More</a>
    </div>
</div>
<?php endif; ?>