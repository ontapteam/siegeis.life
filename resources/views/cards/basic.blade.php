<div class="card {{ \App\Colours::getSecondaryColour() }}">
    <div class="card-content white-text">
        <div class="col parent s12"><span class="card-title {{ \App\Colours::getHeadingTextColor() }}">{{ $title }}</span></div>
        @include("cards.content.".$blade, ['additional_data' => $additional_data])
    </div>
</div>