<div class="box--login">
    Click here to view your statistics
    <br>
    <br>
    <div class="buttons">
        <a href="{{ '/player/xone/' . Auth::user()->name }}" class="waves-effect waves-light btn"><i class="fa fa-bar-chart" aria-hidden="true"></i>My Stats</a>
    </div>
</div>