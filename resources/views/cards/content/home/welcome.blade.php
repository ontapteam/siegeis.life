<div id="welcome">
    <?php if (isset($welcome)) echo $welcome; ?>

    <?php /* ?>
    @foreach($players as $player)
        <?php
        $urls[] = "https://api.r6stats.com/api/v1/players/" . str_replace(" ", "%20", $player->username) . "/?platform=" . $player->platform;
        $season_urls[] = "https://api.r6stats.com/api/v1/players/" . str_replace(" ", "%20", $player->username) . "/seasons/?platform=" . $player->platform;
        ?>
    @endforeach

    <?php
    $urls_json = json_encode($urls, JSON_UNESCAPED_SLASHES);
    $season_urls_json = json_encode($season_urls, JSON_UNESCAPED_SLASHES);
    $urls_halved = array_chunk($urls, ceil(count($urls) / 10));
    $season_urls_halved = array_chunk($season_urls, ceil(count($season_urls) / 10));
    ?>

    <script>

        var i = 1;
        var currentdate = new Date();
        var start = currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();

        console.log(start);

        function end() {
            var endDate = new Date();
            var end = endDate.getHours() + ":"
                + endDate.getMinutes() + ":"
                + endDate.getSeconds();
            console.log(i);
            console.log(time_diff(start, end));
        }

        function time_diff(t1, t2) {
            var parts = t1.split(':');
            var d1 = new Date(0, 0, 0, parts[0], parts[1], parts[2]);
            parts = t2.split(':');
            var d2 = new Date(new Date(0, 0, 0, parts[0], parts[1], parts[2]) - d1);
            // this would also work
            // d2.toTimeString().substr(0, d2.toTimeString().indexOf(' '));
            return (d2.getHours() + ':' + d2.getMinutes() + ':' + d2.getSeconds());
        }

        async function getJsonInParallel(urls) {
            // fetch all the URLs in parallel
            setTimeout(function () {

                const textPromises = urls.map(async url => {
                    const response = await fetch(url, {mode: "no-cors", timeout: 5000});
                    //console.log(i);

                    i++;
                    return response.text();
                });

                // log them in sequence
                for (const textPromise of textPromises) {
                    //console.log(await textPromise);
                }

            }, 500);

        }

        async function getJson(urls) {


            for (const url of urls) {
                const response = await fetch(url, {mode: "no-cors", timeout: 5000});
                //console.log(i);
                //console.log(await response.text());
                i++;
            }

            end();

        }

        <?php
        $x=0; while($x < count($urls_halved)): ?>
        //getJson(<?php echo json_encode($urls_halved[$x], JSON_UNESCAPED_SLASHES); ?>);
        //getJson(<?php echo json_encode($season_urls_halved[$x], JSON_UNESCAPED_SLASHES); ?>);
        <?php $x++; endwhile; ?>

    </script>
     <?php */ ?>

</div>
