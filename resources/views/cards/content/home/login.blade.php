<div class="box--login">
    Login with your existing account details
    <br>
    <br>
    <div class="buttons">
        <a href="/login" class="waves-effect waves-light btn">Login</a>
    </div>
</div>