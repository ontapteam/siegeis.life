<div class="box--login">
    Login with your existing account details
    <br>
    <br>
    <div class="buttons">
        <a class="waves-effect waves-light btn" href="{{ route('logout') }}"
           onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out" aria-hidden="true"></i><span>Logout</span>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
</div>