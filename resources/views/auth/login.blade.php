@extends('layouts.app')

@section('content')
    
    <div class="row">
        <div class="col s12 m10 offset-m1 l8 offset-l2" id="login-form">
            <div class="card grey darken-4">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}" autocomplete="off">
                    <div class="card-content white-text">
                        <div class="row">
                            <div class="col s12">
                                <div class="panel-heading">Login</div>
                                {{ csrf_field() }}
                            </div>

                            <div class="col s12">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                                    <div class="input-field">
                                        <label for="email" class="control-label">E-Mail Address</label>
                                        <input placeholder="Email" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                    </div>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col s12">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                                    <div class="input-field">
                                        <label for="password" class="control-label">Password</label>
                                        <input placeholder="Password" id="password" type="password" class="form-control" name="password" required>
                                    </div>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col s12">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}><label for="remember"></label> Remember Me
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                        <div class="form-group">
                            <div class="row">
                                <div class="col s12">
                                    <button type="submit" class="col s12 btn btn-primary">
                                        Login
                                    </button>

                                    <a class="col s12 btn-link" href="{{ route('password.request') }}">
                                        Forgot Your Password?
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php /*
<div class="container">
    <div class="row">
        <div class="col s12 col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col s8 col-md-4 control-label">E-Mail Address</label>

                            <div class="col s12 col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php */ ?>
@endsection
