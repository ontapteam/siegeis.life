function animateNumbers(element) {
    element.each(function () {

        if (!$(this).hasClass('animated')) {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
            $(this).addClass('animated');
        }

    });
}

const entry = $('.entry');
if (entry.is(':visible')) {
    entry.on('inview', function (event, visible) {
        if (visible === true) {
            // element is now visible in the viewport
            animateNumbers($(this).find('.count'));
        } else {
            // element has gone out of viewport
        }
    });
}

const seasonStats = $('.season-stats');
if (seasonStats.is(':visible')) {
    seasonStats.on('inview', function (event, visible) {
        if (visible === true) {
            // element is now visible in the viewport
            animateNumbers($(this).find('.count'));
        } else {
            // element has gone out of viewport
        }
    });
}