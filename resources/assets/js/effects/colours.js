module.exports = {
    addColours: function () {

        // $("td:contains('')").addClass('green-text');
        // $("td:contains('-')").addClass('red-text').removeClass('green-text');
        //
        // $("td:contains('loss')").addClass('red-text').removeClass('green-text');
        // $("td:contains('win')").addClass('green-text').removeClass('red-text');

        $(".stat-value h2:contains('')").addClass('green-text');
        $(".stat-value h2:contains('-')").addClass('red-text').removeClass('green-text');

        $(".stat-value h2:contains('loss')").addClass('red-text').removeClass('white-text');
        $(".stat-value h2:contains('win')").addClass('white-text').removeClass('red-text');

        $(".stat-value h4:contains('')").addClass('green-text');
        $(".stat-value h4:contains('-')").addClass('red-text').removeClass('green-text');

        $(".stat-value h4:contains('loss')").addClass('red-text').removeClass('green-text');
        $(".stat-value h4:contains('win')").addClass('green-text').removeClass('red-text');

        $('.stat-value h4').each(function () {
            const count = $(this).html();
            if (count === 0) {
                //$(this).addClass('donut');
                $(this).addClass('grey-text text-darken-2');
            }
        });

        $('.value').each(function () {
            const count = $(this).html();
            if (count === 0) {
                //$(this).addClass('donut');
                $(this).addClass('grey-text text-darken-2');
            }
        });
    }
}
