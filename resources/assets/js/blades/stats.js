function showHideStats() {

    var drawer1 = $('#info .collapsible > li').eq(0);
    var drawer2 = $('#info .collapsible > li').eq(1);

    if ($(window).width() <= 600) {

        $('#info .collapsible').collapsible();

        if (drawer1.hasClass('active')) {
            $('#info. collapsible').collapsible('close', 0);
        }

        if (drawer2.hasClass('active')) {
            $('#info .collapsible').collapsible('close', 1);
        }

    } else {

        if (!drawer1.hasClass('active')) {
            $('#info .collapsible').collapsible('open', 0);
        }

        if (!drawer2.hasClass('active')) {
            $('#info .collapsible').collapsible('open', 1);
        }

        $('#info .collapsible').collapsible('destroy');
        
    }
}

setTimeout(function () {
    showHideStats();
}, 500);

$(window).resize(function () {
    clearTimeout(window.resizedFinished);
    window.resizedFinished = setTimeout(function () {
        showHideStats();
    }, 250);
});