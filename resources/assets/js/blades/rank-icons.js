let timer, timer2;

function getSliderSettings() {
    return {
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 5,
        infinite: false,
        dots: true,
        responsive: [
            {
                breakpoint: 1680,
                settings: {
                    centerPadding: '40px',
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 1280,
                settings: {
                    centerPadding: '40px',
                    dots: false,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 990,
                settings: {
                    centerPadding: '40px',
                    dots: false,
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 767,
                settings: {
                    arrows: true,
                    centerPadding: '40px',
                    dots: false,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 600,
                settings: {
                    arrows: true,
                    centerPadding: '40px',
                    dots: false,
                    slidesToShow: 1
                }
            }
        ]
    }
}

function rankIcons() {
    let index = 0;
    let icons = $('.active .rankicons');
    if (icons.is(':visible')) {
        if (!icons.hasClass('slick-initialized')) {
            icons.slick(getSliderSettings()).css('visibility', 'visible').fadeIn();
            clearTimeout(timer2);
            timer2 = setTimeout(function () {
                index = $('.active .current_rank').data('slick-index');
                icons.slick('slickGoTo', index);
            }, 500);
        } else {
            icons.slick('unslick');
            icons.slick(getSliderSettings()).css('visibility', 'visible').fadeIn();
            clearTimeout(timer2);
            timer2 = setTimeout(function () {
                index = $('.active .current_rank').data('slick-index');
                icons.slick('slickGoTo', index);
            }, 500);
        }
    }
}

$('#season-links').find('.tab a').click(function () {
    $('.rankicons').css('visibility', 'hidden');
    $('.count').removeClass('animated');
    clearTimeout(timer);
    timer = setTimeout(function () {
        rankIcons();
    }, 500);
});

setTimeout(function () {
    rankIcons();
}, 250);