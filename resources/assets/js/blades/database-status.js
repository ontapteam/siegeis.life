function updateApiTime() {
    setInterval(function () {

        $.get('/api/time', function (data) {
            $('.snapshot-time .stat-value--no-colour h4').html(data);
        });

        $.get('/api/check', function (data) {
            $('.api-time .stat-value--no-colour h4').html(data);
        });

    }, 1000);
}

if ($('.snapshot-time').is(':visible')) {
    updateApiTime();
}