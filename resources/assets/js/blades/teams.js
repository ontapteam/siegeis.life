$('#addPlayer').click(function () {
    var li = $('.player-names li');
    li.removeClass('reset');
    var clonedLi = li.last().clone(true);
    var lastLi = li.last();
    clonedLi.removeClass('first').addClass('reset').insertAfter(lastLi);
    clonedLi.find('.row-controls > a').remove();
    $('.reset').find('input').val('');
});

$('.remove').click(function () {
    if ($('.player-names li').length > 1) {
        $(this).parent().parent().remove();
    }
});

$('#createTeam').click(function () {
    var players = "";
    $('.player-names ul li').each(function () {
        players += $(this).find('input').val() + ",";
    });
    players = players.slice(0, -1);
    savePlayers(players);
    getTeamPlayers(players);
});

$('#saveTeam').click(function () {
    var players = "";
    $('.player-names ul li').each(function () {
        players += $(this).find('input').val() + ",";
    });
    players = players.slice(0, -1);
    savePlayers(players);
});

function savePlayers(data) {
    var data_prefix = "players=";
    data = data_prefix + data;
    $.get('/team/save', data, function (data) {
        // saved team
    });
}

var colours = require('../effects/colours');

function getTeamPlayers(data) {
    var data_prefix = "players=";
    data = data_prefix + data;
    $('#loading').show();
    $('#results').hide();
    $.get('/team/build', data, function (data) {
        $('#results').html(data).show();
        colours.addColours();
        $('#loading').hide();
    }).fail(function () {
        $('#loading').hide();
    });

}