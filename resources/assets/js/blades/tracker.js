function updateKills(val) {
    if (val == "count") {
        var value = parseInt($('#killCount').val());
        value++;
    } else {
        value = val;
    }
    $('#killCount').attr('value', value);
    $('#endgameKills').attr('value', value);

}

function updateDeaths(val) {

    if (val == "count") {
        var value = parseInt($('#deathCount').val());
        value++;
    } else {
        value = val;
    }

    $('#deathCount').attr('value', value);
    $('#endgameDeaths').attr('value', value);
}

$('#kills').submit(function (ev) {
    ev.preventDefault();
    updateKills("count");
});

$('#deaths').submit(function (ev) {
    ev.preventDefault();
    updateDeaths("count");
});

$('#killCount').keyup(function () {
    var dInput = this.value;
    updateKills(dInput);
});

$('#deathCount').keyup(function () {
    var dInput = this.value;
    updateDeaths(dInput);
});

$('#operatorsList').on('change', function () {
    value = $(this).find(":selected").attr('value');
    $('#endgameOperator').attr('value', value);
});

$('#mapList').on('change', function () {
    value = $(this).find(":selected").attr('value');
    $('#endgameMap').attr('value', value);
});

$('#result').on('change', function () {
    value = $(this).find(":selected").attr('value');
    $('#endgameResult').attr('value', value);
    console.log(value);
});
