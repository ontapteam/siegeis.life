$('#startSession').click(function () {
    $.get('/session/start', function (data) {
        $('#startSession').hide();
        $('#endSession').show();
    });
});

$('#endSession').click(function () {
    $.get('/session/end', function (data) {
        $('#startSession').show();
        $('#endSession').html('Ending session...');
        window.location = "/";
    });
});

$('.session-table thead tr.session-heading').click(function () {
    $(this).parent().next().find('.result-row').toggle();
    $(this).parent().find('.map-heading').toggle();
    $(this).toggleClass('active');
});
