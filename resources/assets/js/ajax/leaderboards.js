function loadLeaderboard() {
    if ($('#top10wins').is(':visible')) {
        $.get('/leaderboards/top10wins', function (data) {
            $('#top10wins').html(data).show();
        });
    }
}

loadLeaderboard();