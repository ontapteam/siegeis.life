/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//require('./libraries/materialize/materialize');

//require('./libraries/jquery');
//require('./libraries/slick-1.6.0/slick/slick');
//require('./libraries/inview');

$(document).ready(function () {
    $('select').material_select();
});

//    require('./blades/nav');

    //require('./blades/video');
    // require('./blades/database-status');
    // require('./blades/rank-icons');
    // require('./blades/stats');
    // require('./blades/teams');
    // require('./effects/count');
    // require('./effects/tabs');
    // require('./ajax/leaderboards');
    // require('./libraries/responsive-tables');
    //var colours = require('./effects/colours');
    //colours.addColours();

//});

window.Vue = require('vue');

// Vue.prototype.headingColour = "cyan-text text-darken-3";

// Global Components
Vue.component('list-item', require('./components/global/ListItem.vue'));

// Home Page Components

// Search Page Components
// Vue.component('search-platform', require('./components/search/SearchPlatform.vue'));

// Player Components
Vue.component('players', require('./components/player/AllPlayers.vue'));
Vue.component('player', require('./components/player/Player.vue'));
Vue.component('player-stats', require('./components/player/Stats.vue'));
Vue.component('player-videos', require('./components/player/Videos.vue'));
Vue.component('player-rank', require('./components/player/Rank.vue'));
Vue.component('player-ratios', require('./components/player/Ratios.vue'));
Vue.component('player-season-ranked-games', require('./components/player/SeasonRankedGames.vue'));
Vue.component('player-ranked-stats', require('./components/player/RankedStats.vue'));
Vue.component('player-casual-stats', require('./components/player/CasualStats.vue'));
Vue.component('video-item', require('./components/player/VideoItem.vue'));

// Daily Templates
Vue.component('player-since-yesterday', require('./components/player/Daily/SinceYesterday.vue'));
Vue.component('player-weekly', require('./components/player/Daily/Weekly.vue'));

// Home Templates
Vue.component('api-status', require('./components/home/ApiStatus.vue'));
Vue.component('database-status', require('./components/home/DatabaseStatus.vue'));
Vue.component('leaderboard', require('./components/home/Leaderboard.vue'));
Vue.component('deadliest-day', require('./components/home/DeadliestDay.vue'));

new Vue({
    el: '#app'
});