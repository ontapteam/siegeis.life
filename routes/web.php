<?php

Auth::routes();

Route::get('/api-test/player/{player}', 'ApiTestController@player');
Route::get('/api-test/player/small/{player}', 'ApiTestController@smallplayer');
Route::get('/api-test/stats/{player}', 'ApiTestController@stats');
Route::get('/api-test/operators/{player}', 'ApiTestController@operators');

// Home
Route::get('/', 'HomeController@index');
Route::get('/profile', 'HomeController@profile');

// Search
Route::get('/search', 'SearchController@home');
Route::post('/search', 'SearchController@index');
Route::get('/search/test', 'SearchController@test');

// Account Urls
Route::resource('account', 'AccountController');
Route::get('account', 'AccountController@account');
Route::patch('account/save', 'AccountController@save');

// Xbox Live
Route::get('/xbox', 'XboxController@index');
Route::get('/xbox/videos/{player}', 'XboxController@index');
Route::get('/xbox/video/{player}/{id}', 'XboxController@getVideo');

// Wordpress Posts
Route::get('/news', 'WordpressController@index');
Route::get('/news/{slug}', 'WordpressController@getPost');
Route::get('/news/category/{category}', 'WordpressController@category');

// Pages
Route::get('about-us', 'WordpressController@getPage');
Route::get('privacy-policy', 'WordpressController@getPage');

// Cron Tasks (used to test cron functions in the browser)
Route::get('cron/daily', 'PlayerController@daily');

// Ajax Endpoints
Route::get('api/time', 'AjaxController@time');
Route::get('api/check', 'AjaxController@getTimeUntilNextApiCheck');

// Player Profile Page
Route::get('player/{platform}/{player}', 'PlayerController@load');
Route::get('player/check/{platform}/{player}', 'PlayerController@check');
Route::get('player/save/{platform}/{player}', 'PlayerController@save');

// Get Endpoints
Route::get('get/player/all', function () {
    $players = \App\Player::all();
    return $players;
});

Route::get('get/player/{platform}/{player}', 'GetController@player');
Route::get('get/stats/{platform}/{player}', 'GetController@stats');
Route::get('get/videos/{player}', 'GetController@videos');
Route::get('get/ranks', 'GetController@ranks');
Route::get('get/sinceyesterday/{player}', 'GetController@sinceyesterday');
Route::get('get/weeks/{player}', 'GetController@weeks');
Route::get('get/leaderboards/{leaderboard}', 'GetController@leaderboards');
Route::get('get/stat/{stat}', 'GetController@stat');
Route::get('get/season/{season}/{platform}/{player}', 'GetController@season');
Route::get('get/seasonEnd/{season}/{platform}/{player}', 'GetController@seasonEnd');