<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('players', function (Blueprint $table) {
            $table->integer('overall_kills')->nullable();
            $table->integer('overall_deaths')->nullable();
            $table->integer('overall_wins')->nullable();
            $table->integer('overall_losses')->nullable();
            $table->double('overall_kd', 15, 3)->nullable();
            $table->integer('overall_kd_diff')->nullable();
            $table->double('overall_wl', 15, 3)->nullable();
            $table->integer('overall_wl_diff')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('players', function (Blueprint $table) {
            //
        });
    }
}
