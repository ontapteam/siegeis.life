<?php

namespace Deployer;

require 'recipe/laravel.php';

add('writable_dirs', []);
set('repository', 'git@bitbucket.org:ontapteam/siegeis.life.git');
set('git_recursive', false);
set('default_stage', 'production');

host('siegeis.life')
    ->user('root')
    ->stage('production')
    ->roles('app')
    ->set('shared_dirs', array(
        'storage',
        'blog/wp-content/uploads'
    ))
    ->set('shared_files', array(
        '.env',
	'api_ticket',
        'blog/wp-config.php',
        'public/robots.txt',
        'public/manifest.json'
    ))
    ->set('deploy_path', '/var/www/siege');

desc('Restart PHP-FPM service');
task('php-fpm:restart', function () {
    // The user must have rights for restart service
    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
    run('service php7.0-fpm restart');
    run('sudo chown -R www-data:www-data /var/www');
});
after('deploy:symlink', 'php-fpm:restart');

