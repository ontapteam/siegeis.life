####How to update the colour scheme

Edit the following file:
`resources/assets/sass/settings/_colours.scss`

Use the following resource for colours:
`https://materializecss.com/color.html`

