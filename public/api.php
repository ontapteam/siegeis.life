<?php
/*
    AUTHOR: Andreas "Nekres" G.
    FILE: rainbowsix7.php
*/

//$request = strtolower($_GET['command']);
//if (!$request) { echo '\'&command=\' parameter not defined! (Options: \'rank\', \'stats\' or \'time\')'; return; };
//
//$platform = strtolower($_GET['platform']);
//if (!in_array($platform, array('uplay', 'xbl', 'psn'))) { echo '\'&platform=\' parameter not correct! (Options: \'xbl\', \'psn\' or \'uplay\')'; return; };
//

$nick = "durrtydogg";
$platform = "xbl";

if ($platform == 'uplay') {
    $machine = 'PC';
    $path_id = ['5172a557-50b5-4665-b7db-e3f2e8c5041d', 'PC'];
};
if ($platform == 'xbl') {
    $machine = 'XONE';
    $path_id = ['98a601e5-ca91-4440-b1c5-753f601a2c90', 'XBOXONE'];
};
if ($platform == 'psn') {
    $machine = 'PS4';
    $path_id = ['05bfb3f7-6c21-4c42-be1f-97a33fb5cf66', 'PS4'];
};
//
//$nick = $_GET['nick'];
//if (!$nick) { echo '\'&nick=\' parameter not defined!'; return; };


function _postLOGIN() {
    $ch = curl_init('https://connect.ubi.com/ubiservices/v2/profiles/sessions');
    # Setup request to send json via POST.
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(unserialize(base64_decode("YToyOntzOjU6ImxvZ2luIjtzOjE1OiJOZWtyZXNEZXZlbG9wZXIiO3M6ODoicGFzc3dvcmQiO3M6MTU6IlU1bnJLaDRXYjVlbkdUWSI7fQ=="))));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: */*', 'Content-Type: application/json; charset=UTF-8', 'Authorization: Basic YW5kcmVhcy5nYWVydG5lcjkzQGdtYWlsLmNvbTpVcGxheTMsMTQx', 'Ubi-AppId: 39baebad-39e5-4552-8c25-2c9b919064e2', 'Ubi-RequestedPlatformType: uplay', 'Connection: keep-alive', 'Keep-Alive: timeout 0, max 0'));
    # Return response instead of printing.
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    # Send request.
    $result = curl_exec($ch);
    curl_close($ch);
    apcu_store('uplayconnect_ticket', json_decode($result, true)['ticket']);
    //return json_decode($result, true)['ticket'];
};
function _getJSON($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json, text/plain, */*', 'Authorization: Ubi_v1 t='.apcu_fetch('uplayconnect_ticket'), 'Ubi-AppId: 39baebad-39e5-4552-8c25-2c9b919064e2', 'Connection: keep-alive', 'Keep-Alive: timeout 0, max 0'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // so curl_exec returns data

    // grab URL and pass it to the browser; store returned data
    $curlRes = curl_exec($ch);
    // close cURL resource, and free up system resources
    curl_close($ch);
    // Decode returned JSON so it can be handled as a multi-dimensional associative array
    return json_decode($curlRes, true);
};
// If ticket cache doesn't exist initially, try to login until it does.
while (!apcu_exists('uplayconnect_ticket')) { _postLOGIN(); };

$access = _getJSON('https://api-ubiservices.ubi.com/v2/profiles?nameOnPlatform='.$nick.'&platformType='.$platform);

// If ubi services throw an error, try requesting a new ticket until success.
while (isset($access['httpCode']) || isset($access['errorCode'])) { _postLOGIN(); };


echo "<pre>";
var_dump($access);
echo "</pre>";
exit;


// Fetch player data from JSON.
$userId = $access['profiles'][0]['userId'];
$profileId = $access['profiles'][0]['profileId'];

echo "3a23b6ab-4c02-450b-822b-4bcbafed3be1 <br>";
echo $userId. "<br>";
$profileId = "66206e18-715a-443e-aebd-2de1f5779bc0";


var_dump(_getJSON('https://public-ubiservices.ubi.com/v1/spaces/98a601e5-ca91-4440-b1c5-753f601a2c90/sandboxes/OSBOR_XBOXONE_LNCH_A/r6karma/players?board_id=pvp_ranked&profile_ids=66206e18-715a-443e-aebd-2de1f5779bc0,2a7552ff-0887-4040-9d6a-2379e17993de&region_id=emea&season_id=2'));
exit;

if (empty($userId) or empty($profileId)) {
    if ( $platform == "uplay" ) {
        echo 'Couldn\'t retrieve \'userId\' or \'profileId\'! Maybe player doesn\'t exist? Check typos.';
        return;
    }
    echo 'Couldn\'t retrieve \'userId\' or \'profileId\'! Maybe player doesn\'t exist? Check typos or try account nick instead of gamer tag (Ie. first nick on account creation.).';
    return;
}

/**
 *	Command query: &command=stats
 *	Description: Returns general statistics of the player.
 */
if ($request == 'stats') {

    $data = _getJSON('https://public-ubiservices.ubi.com/v1/spaces/'.$path_id[0].'/sandboxes/OSBOR_'.$path_id[1].'_LNCH_A/playerstats2/statistics?populations='.$profileId.'&statistics=rankedpvp_matchwon,rankedpvp_matchlost,rankedpvp_timeplayed,rankedpvp_matchplayed,rankedpvp_kills,rankedpvp_death');

    $wins = intval($data['results'][$profileId]['rankedpvp_matchwon:infinite']);
    $losses = intval($data['results'][$profileId]['rankedpvp_matchlost:infinite']);
    $totalGames = intval($wins + $losses);
    //$totalGames = intval($data['results'][$profileId]['rankedpvp_matchplayed:infinite']);
    if ($totalGames > 0) {
        $winratio = floatval(($wins/$totalGames)*100);
        $lossratio = floatval(($losses/$totalGames)*100);
        $wlRatio = round($winratio/$lossratio, 1);
    } else {
        $wlRatio = 0.0;
    }

    $kills = intval($data['results'][$profileId]['rankedpvp_kills:infinite']);
    $deaths = intval($data['results'][$profileId]['rankedpvp_death:infinite']);
    if ($deaths > 0) {
        $kdRatio = round($kills/$deaths, 1);
    } else {
        $kdRatio = round($kills, 1);
    }

    $lvl = _getJSON('https://public-ubiservices.ubi.com/v1/spaces/'.$path_id[0].'/sandboxes/OSBOR_'.$path_id[1].'_LNCH_A/r6playerprofile/playerprofile/progressions?profile_ids='.$profileId);
    echo 'Lv.'.$lvl['player_profiles'][0]['level'].' | '.$wlRatio.' W/L ratio | '.$kdRatio.' K/D ratio';

};

function convertTime($dec) {
    $t = round($dec);
    return sprintf('%02dh %02dm %02ds', ($t/3600),($t/60%60), $t%60);
};

/**
 *	Command query: &command=time
 *	Description: Returns the time spent in ranked plus casual multiplayer.
 */
if ($request == 'time') {

    $data = _getJSON('https://public-ubiservices.ubi.com/v1/spaces/'.$path_id[0].'/sandboxes/OSBOR_'.$path_id[1].'_LNCH_A/playerstats2/statistics?populations='.$profileId.'&statistics=casualpvp_timeplayed,rankedpvp_timeplayed');
    $ranked = $data['results'][$profileId]['rankedpvp_timeplayed:infinite'];
    $casual = $data['results'][$profileId]['casualpvp_timeplayed:infinite'];
    echo convertTime($ranked + $casual).' (ranked + casual)';

};

$RSIX7_tiers = ['Copper Ⅳ', 'Copper Ⅲ', 'Copper Ⅱ', 'Copper Ⅰ',
    'Bronze Ⅳ', 'Bronze Ⅲ', 'Bronze Ⅱ', 'Bronze Ⅰ',
    'Silver Ⅳ', 'Silver Ⅲ', 'Silver Ⅱ', 'Silver Ⅰ',
    'Gold Ⅳ', 'Gold Ⅲ', 'Gold Ⅱ', 'Gold Ⅰ', 'Platinum Ⅲ',
    'Platinum Ⅱ', 'Platinum Ⅰ', 'Diamond ♦'];

$RSIX7_regions = ['EU' => 'emea', 'NA' => 'ncsa', 'ASIA' => 'apac'];

/**
 *	Command query: &command=rank
 *	Optional query: &region=EU|NA|ASIA
 *	Description: Returns current season placement and matchmaking rating of the player.
 */
if ($request == 'rank') {
    if ($_GET['region'] && in_array(strtoupper($_GET['region']), array_keys($RSIX7_regions))) {
        $RSIX7_regions = array(strtoupper($_GET['region']) => $RSIX7_regions[strtoupper($_GET['region'])]);
    };
    for ($i = 0; $i < count(array_values($RSIX7_regions)); $i++) {
        $data = _getJSON('https://public-ubiservices.ubi.com/v1/spaces/'.$path_id[0].'/sandboxes/OSBOR_'.$path_id[1].'_LNCH_A/r6karma/players?board_id=pvp_ranked&profile_ids='.$profileId.'&region_id='.array_values($RSIX7_regions)[$i].'&season_id=-1');
        $rank = intval($data['players'][$profileId]['rank']);
        if ($rank !== 0) {
            echo $RSIX7_tiers[$rank - 1].' - matchmaking rating: '.intval($data['players'][$profileId]['mmr']);
            return;
        }
    };
    echo 'Player still has to do ranked placement matches OR the profile doesn\'t exist on the specified platform.';
};
?>