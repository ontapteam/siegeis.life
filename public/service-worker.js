
var CACHE_NAME = 'sil-v8.0';
var urlsToCache = [
    'css/app.css',
    'js/analytics.js',
    'images/siege-symbol.png',
    'images/wallpaper.jpg',
    'https://fonts.googleapis.com/css?family=Miriam+Libre:400,700|Roboto+Condensed:300,400,700|Open+Sans|Oswald',
    'https://fonts.gstatic.com/s/robotocondensed/v16/Zd2E9abXLFGSr9G3YK2MsDAdhzWOYhqHvOZMRGaEyPo.woff2',
    'https://fonts.gstatic.com/s/oswald/v16/pEobIV_lL25TKBpqVI_a2w.woff2'
];

self.addEventListener('install', function (event) {

    // Perform install steps
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function (cache) {
                //console.log('Opened cache');
                return cache.addAll(urlsToCache);
            })
    );

    var cacheWhitelist = [CACHE_NAME]; 

    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                cacheNames.map(function(cacheName) {
                    if (cacheWhitelist.indexOf(cacheName) === -1) {
                        console.log('Deleting old cache: ' + cacheName);
                        return caches.delete(cacheName);
                    }
                })
            );
        })
    );


});

self.addEventListener('fetch', function(event) {
    console.log('Fetch event for', event.request.url);
    event.respondWith(
        caches.match(event.request).then(function(response) {

            /*
            if (response) {
                console.log('CACHE: Found', event.request.url);
                return response;
            } else {
                //console.log('Network request for', event.request.url);
                return fetch(event.request);
            }
            */

            return fetch(event.request);

            // TODO 4 - Add fetched files to the cache

        }).catch(function(error) {

            // TODO 6 - Respond with custom offline page

        })
    );
});

self.addEventListener('activate', function(event) {
    console.log('Activating new service worker...');

    /*
    var cacheWhitelist = [CACHE_NAME];

    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                cacheNames.map(function(cacheName) {
                    if (cacheWhitelist.indexOf(cacheName) === -1) {
                        console.log('Deleting old cache: ' + cacheName);
                        return caches.delete(cacheName);
                    }
                })
            );
        })
    );
    */

});
