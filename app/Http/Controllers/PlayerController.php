<?php

namespace App\Http\Controllers;

use \Cache;
use App\Services\R6API;
use App\Player;
use App\Stat;
use Illuminate\Http\Request;
use GuzzleHttp\Promise\EachPromise;
use GuzzleHttp\PromiseInterface;
use GuzzleHttp\ResponseInterface;
use Guzzle\Common\Exception\MultiTransferException;

class PlayerController extends Controller
{
    private $api;

    /**
     * PlayerController constructor.
     * @param R6API $api
     */
    public function __construct(R6API $api)
    {
        $this->api = $api;
    }

    /**
     * Load a player from the database and get their relevant stats
     *
     * @param $platform
     * @param $username
     * @return array|mixed
     */
    public function load($platform, $username)
    {
        $player = Player::where('platform', $platform)->where('username', $username)->get()->first();
        if($player) {
            $yesterday = Stat::getDailyPlayerStats($player->username);
            $weekly    = Stat::getWeeklyPlayerStats($player->username);

            return view('pages.player', compact('player', 'yesterday', 'weekly'));
        } else {
            return redirect('/');
        }
    }

    /**
     * Check to see if the player exists in the database
     * @param $platform
     * @param $username
     * @return bool
     */
    public function check($platform, $username) {
        if($player = Player::where('platform', $platform)->where('username', $username)->get()->first()){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Save the player to the database
     * @param Request $request
     */
    public function save(Request $request)
    {
        $newPlayer = Player::firstOrCreate(['username' => $request->player]);
        $newPlayer->platform = $request->platform;
        $newPlayer->save();
    }

    /**
     * Test function for cron tasks
     */
    public function daily()
    {
        $player = new Player();
        $player->dailyStats();
    }

    /**
     * Return all the players the "all" view
     *
     * @return array|mixed
     */
    public function all()
    {
        $players = Player::all();
        return view('all', compact('players'));
    }

    /**
     * API Test Function
     */
    public function test()
    {
        $players = Player::all();
        $promises = (function () use ($players) {
            $client = new \GuzzleHttp\Client();
            foreach ($players as $player) {
                yield $client->getAsync('https://api.r6stats.com/api/v1/players/' . $player->username . '/?platform=' . $player->platform . '');
            }
        })();

        $each = new EachPromise($promises, [
            'concurrency' => 15,
            'fulfilled' => function ($response) {
                if ($profile = json_decode($response->getBody()->getContents(), true)) {
                    //echo "<pre>";
                    //echo "Success :)" . "\n";
                    ///$this->ddd($profile);
                    //echo "</pre>";
                }
            },
            'rejected' => function ($response) {
                //echo "<pre>";
                //echo "Error :(" . "\n";
                //$this->ddd($response->getMessage());
                //echo "</pre>";
            }
        ]);

        $each->promise()->wait();

        echo((microtime(true) - LARAVEL_START) / 60);

        //return view('pages.player.ajax', compact('players', 'debugbarRenderer'));

    }

    /**
     * Get player
     *
     * @param $platform
     * @param $player
     * @return array|mixed
     *
     * @deprecated
     */
    public function getplayer($platform, $player)
    {
        $_player = Player::where('id', 1)->get();

        $player = $_player;
        if (count($_player->seasons) > 0) {
            return view('pages.player.ranked', compact('player'));
        } else {
            return view('pages.player.casual', compact('player'));
        }

    }


}

