<?php

namespace App\Http\Controllers;

use Auth;
use App\Player;
use GuzzleHttp\Client as Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Promise\EachPromise;
use GuzzleHttp\PromiseInterface;
use GuzzleHttp\ResponseInterface;

use Guzzle\Common\Exception\MultiTransferException;

use Illuminate\Support\Facades\DB as DB;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    public function test()
    {
        $username = "eltish";
        $search   = true;
        return view('pages.search.platforms', compact('username', 'search', 'player'));
    }

    public function index(Request $request)
    {
        $username = str_replace("%20", " ", $request->player);
        try {

            $client = new Client;

            $this->api = new \App\Services\R6API;

            $platform = "uplay";

            if ($request->platform == "xone") {
                $platform = "xbl";
            }

            if ($request->platform == "ps4") {
                $platform = "psn";
            }

            $request = new \GuzzleHttp\Psr7\Request('GET', url('/') . '/api-test/player/' . $username . '?appcode=test&platform=' . $platform);
            $promise = $client->sendAsync($request)->then(function ($response) {
                $data   = json_decode($response->getBody());
                $result = reset($data->players);

                // Check the database for a player with the same uplay id
                $player = \App\Player::where('uplay_id', $result->profile_id)->first();

                if (isset($player)) {
                    $player->username = $result->nickname;
                    $player->save();
                } else {
                        (new \App\Player)->updateOrCreate(
                            [
                                'username' => $result->nickname,
                                'platform' => $result->platform,
                                'uplay_id' => $result->profile_id
                            ]
                        );
                    }

                });

                $promise->wait();

                return redirect('/player/' . $platform . '/' . $username);

            } catch (\Exception $e) {
                var_dump($e->getMessage());
                return redirect('/')->with('message', 'Player not found :(');
            }


    }

}
