<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Services\XBOX;

class XboxController extends Controller
{

    public function index($player)
    {
        $xbox = new XBOX($player);
        $videos = $xbox->getFormattedVideoData();
        return view('pages.xbox.videos', compact('videos'));
    }

    public function getVideo($player, $id)
    {
        $xbox = new XBOX($player);
        $videos = $xbox->getFormattedVideoData();
        $video = $videos[$id];
        return view('pages.xbox.video', compact('video'));
    }
}
