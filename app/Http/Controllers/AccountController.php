<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    protected $user;

    /**
     * AccountController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $players = new Players();
        $api = new R6API();
        $best = new Best();
        $database = new Database();

        return view('pages.home', compact('players', 'api', 'best', 'database'));
    }

    /**
     * Edit the users profile
     *
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $user = User::whereId($id)->firstOrFail();
        return view('pages.account.edit', compact('user'));
    }

    /**
     * Update the users profile (Save)
     *
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request)
    {

        $user = Auth::getUser();
        $user->name = $request->input('name');
        $user->update();

        $url = '/account/' . $user->id . '/edit';
        return redirect($url);

    }

}
