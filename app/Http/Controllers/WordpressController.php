<?php

namespace App\Http\Controllers;

use App\Wordpress\WordpressPost;
use App\Wordpress\WordpressPage;
use App\Wordpress\Breadcrumbs;
use Illuminate\Http\Request;

/**
 * @property WordpressPost post
 */
class WordpressController extends Controller
{

    /**
     * WordpressController constructor.
     */
    public function __construct()
    {
        $this->post = new WordpressPost();
        $this->wpPage = new WordpressPage();
        $this->breadcrumbs = new Breadcrumbs();
    }

    public function category($category)
    {
        $news = $this->post->getNews($category);
        $cats = $this->post->getCategories();
        $breadcrumbs = $this->breadcrumbs->getBreadcrumbs();
        return view('pages.news', compact('news', 'cats', 'breadcrumbs'));
    }

    public function categoryPage($category, $page)
    {
        $news = $this->post->getNews($category, $page);
        $cats = $this->post->getCategories();
        return view('pages.news', compact('news', 'cats'));
    }

    public function index()
    {
        $news = $this->post->getNews();
        $cats = $this->post->getCategories();
        $breadcrumbs = $this->breadcrumbs->getBreadcrumbs();
        return view('pages.news', compact('news', 'cats', 'breadcrumbs'));
    }

    public function getPost($slug)
    {
        $post = $this->post->getPost($slug);
        $cats = $this->post->getCategories();
        $breadcrumbs = $this->breadcrumbs->getBreadcrumbs();
        return view('pages.blog.post', compact('post', 'cats', 'breadcrumbs'));
    }

    public function getPage()
    {
        $pageName = request()->segment(1);
        $post = $this->wpPage->getPage($pageName);
        $cats = $this->post->getCategories();
        $breadcrumbs = $this->breadcrumbs->getBreadcrumbs();
        return view('pages.blog.post', compact('post', 'cats', 'breadcrumbs'));
    }

}
