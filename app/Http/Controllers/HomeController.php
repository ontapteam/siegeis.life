<?php

namespace App\Http\Controllers;

//use App\Best;
use App\Database;
//use App\Leaderboard;
//use App\Players;
//use App\Services\R6API;
use App\Wordpress\WordpressPost;
use Illuminate\Http\Request;
use Mockery\Exception;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
//        $this->players = new Players();
//        $this->api = new R6API();
//        $this->best = new Best();
//        $this->database = new Database();
//        $this->leaderboard = new Leaderboard();
        $this->post = new WordpressPost();
//        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

//        $players = $this->players;
//        $api = $this->api;
//        $best = $this->best;
//        $database = $this->database;
//        $top10wins = $this->leaderboard->getTop10Wins();
//        $welcome = $this->post->getWelcomePost();
//        $featured = $this->post->getFeaturedPost();

        try {
            $welcome = $this->post->getWelcomePost();
            return view('pages.home', compact('players', 'welcome'));
        } catch (Exception $e) {

        }

        // return view('pages.home', compact('players', 'api', 'best', 'database', 'top10wins', 'featured', 'welcome'));

    }

}
