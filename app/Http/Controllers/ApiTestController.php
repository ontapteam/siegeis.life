<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\StatsAPI;

/**
 * @property array final
 * @property StatsAPI api
 */
class ApiTestController extends Controller
{
    public function __construct()
    {
        $this->api = new StatsAPI();
    }

    public function player($player)
    {
        echo $this->api->getPlayer($player);
    }

    public function smallplayer($player)
    {
        echo $this->api->getSmallPlayer($player);
    }

    public function stats($player)
    {
        echo $this->api->getStats($player);
    }

    public function operators($player)
    {
        echo $this->api->getOperators($player);
    }

}
