<?php

namespace App\Http\Controllers;

use App\Database;
use App\Services\R6API;
use Illuminate\Http\Request;

/**
 * @property Database database
 * @property R6API api
 */
class AjaxController extends Controller
{

    /**
     * AjaxController constructor.
     */
    public function __construct()
    {
        $this->database = new Database();
        $this->api = new R6API();
    }

    public function time()
    {
        echo $this->database->getTimeUntilNextDailySnapshot();
    }

    public function getTimeUntilNextApiCheck()
    {
        echo $this->api->getTimeUntilNextApiCheck();
    }

}
