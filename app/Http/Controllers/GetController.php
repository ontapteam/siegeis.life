<?php

namespace App\Http\Controllers;

use App\Stats\Rank;
use Illuminate\Http\Request;
use App\Player;
use App\Stat;
use App\Leaderboard;
use App\Services\XBOX;

class GetController extends Controller
{

    public function player($platform, $username)
    {
        $player = Player::where('platform', $platform)->where('username', $username)->get()->first();
        return $player;
    }

    public function stats($platform, $username)
    {
        $player = $this->player($platform, $username);
        $stats = Stat::where('player_id', $player->id)->latest()->get()->first();
        return $stats;
    }

    public function videos($username)
    {
        $xboxApi = new XBOX($username);
        $videos = $xboxApi->getRawVideos();
        return array_slice($videos, 0, 5);
    }

    public function ranks()
    {
        $ranks = Rank::getAvailableRanks();
        return $ranks;
    }

    public function sinceyesterday($player)
    {
        return Stat::getDailyPlayerStats($player);
    }

    public function weeks($player)
    {
        return Stat::getWeeklyPlayerStats($player);
    }

    public function seasonEnd($season, $platform, $player)
    {
        $seasonEnd = Stat::getSeasonEnd($season, $platform, $player);
        if (isset($seasonEnd->kills)) {
            return $seasonEnd;
        } else {
            return 0;
        }
    }

    public function leaderboards($leaderboard)
    {
        switch ($leaderboard) {
            case "seasonwins":
                $leaderboard = Leaderboard::getTop10Wins();
                break;
        }

        return $leaderboard;
    }

    public function stat($stat)
    {
        switch ($stat) {
            case "deadliestday":
                $stat = Stat::calculateDeadliestDay();
                break;
        }

        return $stat;
    }

    public function season($season, $platform, $player)
    {
        $seasonEnd = Stat::getSeasonEnd($season, $platform, $player);
        if ($seasonEnd) {
            $currentStats = Stat::getDailyPlayerStats($player);
            $seasonKillsSoFar = $seasonEnd->kills - $currentStats->kills;
            $seasonDeathsSoFar = $seasonEnd->deaths - $currentStats->deaths;
            $seasonKd = round($seasonKillsSoFar / $seasonDeathsSoFar, 3);
        } elseif ($seasonEnd == null) {
            $currentStats = Stat::getDailyPlayerStats($player);
            $seasonKillsSoFar = 0 - $currentStats->kills;
            $seasonDeathsSoFar = 0 - $currentStats->deaths;
            $seasonKd = round($seasonKillsSoFar / $seasonDeathsSoFar, 3);
        } else {
            $seasonKd = 0;
        }

        return (float)$seasonKd;

    }

}
