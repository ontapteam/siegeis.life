<?php

namespace App;

use Auth;
use Request;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $url;

    /**
     * Menu constructor.
     */
    public function __construct()
    {
        $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $this->url = $url;
        $this->menu = Auth::user() ? $this->getLoggedInMenu() : $this->getLoggedOutMenu();
    }

    /**
     * Get active menu item
     *
     * @param $item
     * @return bool|string
     */
    public static function getActive($item)
    {
        $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        if (strpos($url, 'player') !== false && $item == "player") {
            return "active";
        } elseif ("/" . $item === $_SERVER['REQUEST_URI']) {
            return "active";
        } elseif (strpos(Request::path(), strtolower($item))) {
            return "active";
        } else {
            return false;
        }
    }

    public function getLoggedInMenu()
    {
        $menu = array(
            array('my stats', '/player/xone/' . Auth::user()->name, $this->getActive('player'), 'bar-chart'),
            //array('team', '/team', $this->getActive('team'), 'users'),
            array('news', '/news', "link--news " . $this->getActive('news'), 'newspaper-o'),
            //array('maps', '/maps', "link--maps " . $this->getActive('maps'), 'map'),
            //array('leaderboards', '/leaderboards', "link--leaderboardds " . $this->getActive('operators'), 'trophy'),
            //array('operators', '/operators', "link--operators " . $this->getActive('operators'), 'male'),
            //array('weapons', '/weapons', "link--weapons" . $this->getActive('weapons'), 'flash')
            //array('operators', '/operators/xone/' . Auth::user()->name, $this->getActive('operators'), 'users')
        );
        return $menu;
    }

    public function getLoggedOutMenu()
    {
        $menu = array(
            array('news', '/news', "link--news " . $this->getActive('news'), 'newspaper-o'),
            //array('maps', '/maps', "link--maps " . $this->getActive('maps'), 'map'),
            //array('leaderboards', '/leaderboards', "link--leaderboardds " . $this->getActive('operators'), 'trophy'),
            //array('operators', '/operators', "link--operators " . $this->getActive('operators'), 'male'),
            //array('weapons', '/weapons', "link--weapons" . $this->getActive('weapons'), 'flash'),
            array('login', '/login', "link--login " . $this->getActive('login'), 'user'),
            array('register', '/register', "link--register " . $this->getActive('register'), 'pencil')
        );
        return $menu;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

}
