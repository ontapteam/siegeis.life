<?php

namespace App;

use App\Player;
use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    protected $fillable = ['player_id', 'user_id'];

    /**
     * @return array
     */
    public static function getAvailableRanks()
    {
        $availableRanks = array(
            0 => array('Copper IV', 1300),
            1 => array('Copper III', 1400),
            2 => array('Copper II', 1500),
            3 => array('Copper I', 1600),
            4 => array('Bronze IV', 1700),
            5 => array('Bronze III', 1800),
            6 => array('Bronze II', 1900),
            7 => array('Bronze I', 2000),
            8 => array('Silver IV', 2100),
            9 => array('Silver III', 2200),
            10 => array('Silver II', 2300),
            11 => array('Silver I', 2400),
            12 => array('Gold IV', 2500),
            13 => array('Gold III', 2700),
            14 => array('Gold II', 2900),
            15 => array('Gold I', 3100),
            16 => array('Platinum III', 3300),
            17 => array('Platinum II', 3700),
            18 => array('Platinum I', 4100),
            19 => array('Diamond', 4500)
        );
        return $availableRanks;
    }

    /**
     * @param null $player
     * @return mixed
     */
    public static function getDailyPlayerStats($player = null)
    {
        $player = str_replace("%20", " ", $player);

        if (isset($player)) {
            $player = Player::where('username', $player)->get();
            if (count($player) > 0) {
                $daily = self::where('player_id', $player->first()->id)
                    ->orderBy('created_at', 'desc')->get();

                $yesterday = $daily->get(0);
            }
        } elseif (auth()->user()) {
            $daily = self::where('user_id', auth()->user()->id)
                ->orderBy('created_at', 'desc')->get();

            $yesterday = $daily->get(0);
        }

        return $yesterday;

    }

    /**
     * @param null $player
     * @return mixed
     */
    public static function getWeeklyPlayerStats($player = null)
    {
        $daily = array();
        $week = 1;
        $day = 0;

        $player = str_replace("%20", " ", $player);

        if (isset($player)) {
            $player = Player::where('username', $player)->get();
            if (count($player) > 0) {
                $daily = self::where('player_id', $player->first()->id)
                    ->orderBy('created_at', 'desc')
                    ->get();
            }
        } elseif (auth()->user()) {
            $daily = self::where('user_id', auth()->user()->id)
                ->orderBy('created_at', 'desc')
                ->get();
        }

        if (isset($daily)) {

            $totalKills = 0;
            $totalDeaths = 0;
            $totalWins = 0;
            $totalLosses = 0;
            $totalKdDiff = 0;
            $totalWlDiff = 0;
            $totalPoints = 0;

            foreach ($daily as $stat) {

                if ($day < (count($daily) - 1)) {

                    $kills = $daily[$day]['kills'] - $daily[$day + 1]['kills'];
                    $deaths = $daily[$day]['deaths'] - $daily[$day + 1]['deaths'];
                    $wins = $daily[$day]['wins'] - $daily[$day + 1]['wins'];
                    $losses = $daily[$day]['losses'] - $daily[$day + 1]['losses'];
                    $kddiff = $kills - $deaths;
                    $wldiff = $wins - $losses;
                    $kd = self::calculateKd($kills, $deaths);
                    $wl = self::calculateKd($wins, $losses);
                    $points = $daily[$day]['points'] - $daily[$day + 1]['points'];

                } else {

                    $kills = $daily[$day]['kills'] - $daily[$day]['kills'];
                    $deaths = $daily[$day]['deaths'] - $daily[$day]['deaths'];
                    $wins = $daily[$day]['wins'] - $daily[$day]['wins'];
                    $losses = $daily[$day]['losses'] - $daily[$day]['losses'];
                    $kddiff = $kills - $deaths;
                    $wldiff = $wins - $losses;
                    $kd = self::calculateKd($kills, $deaths);
                    $wl = self::calculateKd($wins, $losses);
                    $points = $daily[$day]['points'] - $daily[$day]['points'];


                }

                $data['weekly'][$week]['days'][$day]['date'] = date("D jS F Y", strtotime('-1 day', strtotime($stat->created_at)));
                //$data['weekly'][$week]['days'][$day]['date'] = date("D jS F Y", strtotime($stat->created_at));

                $data['weekly'][$week]['days'][$day]['kills'] = $kills;
                $data['weekly'][$week]['days'][$day]['deaths'] = $deaths;
                $data['weekly'][$week]['days'][$day]['wins'] = $wins;
                $data['weekly'][$week]['days'][$day]['losses'] = $losses;
                $data['weekly'][$week]['days'][$day]['kddiff'] = $kddiff;
                $data['weekly'][$week]['days'][$day]['wldiff'] = $wldiff;
                $data['weekly'][$week]['days'][$day]['kd'] = $kd;
                $data['weekly'][$week]['days'][$day]['wl'] = $wl;
                $data['weekly'][$week]['days'][$day]['points'] = $points;

                $totalKills += $data['weekly'][$week]['days'][$day]['kills'];
                $totalDeaths += $data['weekly'][$week]['days'][$day]['deaths'];
                $totalWins += $data['weekly'][$week]['days'][$day]['wins'];
                $totalLosses += $data['weekly'][$week]['days'][$day]['losses'];
                $totalKdDiff += $data['weekly'][$week]['days'][$day]['kddiff'];
                $totalWlDiff += $data['weekly'][$week]['days'][$day]['wldiff'];
                $totalPoints += $data['weekly'][$week]['days'][$day]['points'];

                $data['weekly'][$week]['totals']['kills'] = $totalKills;
                $data['weekly'][$week]['totals']['deaths'] = $totalDeaths;
                $data['weekly'][$week]['totals']['wins'] = $totalWins;
                $data['weekly'][$week]['totals']['losses'] = $totalLosses;
                $data['weekly'][$week]['totals']['kddiff'] = $totalKdDiff;
                $data['weekly'][$week]['totals']['wldiff'] = $totalWlDiff;
                $data['weekly'][$week]['totals']['kd'] = self::calculateKd($totalKills, $totalDeaths);
                $data['weekly'][$week]['totals']['wl'] = self::calculateKd($totalWins, $totalLosses);
                $data['weekly'][$week]['totals']['points'] = $totalPoints;

                $data['totals'][$week][$day] = $stat;

                if (date('D', strtotime($stat->created_at)) === 'Tue'):
                    $week++;
                    $totalKills = 0;
                    $totalDeaths = 0;
                    $totalWins = 0;
                    $totalLosses = 0;
                    $totalKdDiff = 0;
                    $totalWlDiff = 0;
                    $totalKd = 0;
                    $totalWl = 0;
                    $totalPoints = 0;
                endif;

                $day++;
            }
        }

        if (isset($data)) {
            return $data;
        }

    }

    /**
     * @return int
     */
    public static function calculateDeadliestDay()
    {

        $all = self::all();
        $kills = array(
            'monday_kills' => 0,
            'tuesday_kills' => 0,
            'wednesday_kills' => 0,
            'thursday_kills' => 0,
            'friday_kills' => 0,
            'saturday_kills' => 0,
            'sunday_kills' => 0
        );

        foreach ($all as $stat) {

            $date_stamp = strtotime(date('Y-m-d', strtotime($stat->created_at)));

            if (date('D', $date_stamp) === 'Mon') {
                $kills['monday_kills'] += $stat->kills;
            }

            if (date('D', $date_stamp) === 'Tue') {
                $kills['tuesday_kills'] += $stat->kills;
            }

            if (date('D', $date_stamp) === 'Wed') {
                $kills['wednesday_kills'] += $stat->kills;
            }

            if (date('D', $date_stamp) === 'Thur') {
                $kills['thursday_kills'] += $stat->kills;
            }

            if (date('D', $date_stamp) === 'Fri') {
                $kills['friday_kills'] += $stat->kills;
            }

            if (date('D', $date_stamp) === 'Sat') {
                $kills['saturday_kills'] += $stat->kills;
            }

            if (date('D', $date_stamp) === 'Sun') {
                $kills['sunday_kills'] += $stat->kills;
            }

        }

        $day = array_keys($kills, max($kills));

        $best_day = ucwords(str_replace('_kills', '', $day[0]));
        return $best_day;
    }

    /**
     * @param $kills
     * @param $deaths
     * @return float|int
     */
    public static function calculateKd($kills, $deaths)
    {
        if ($kills > 0 && $deaths > 0) {
            $kd = round(((float)$kills / (float)$deaths), 3);
            return $kd;
        } else {
            return $kills;
        }
    }

    /**
     * @param $season
     * @param $platform
     * @param $player
     * @return mixed
     */
    public static function getSeasonEnd($season, $platform, $player)
    {
        $seasons[1] = "2017-12-05";
        $seasons[2] = "2017-12-05";
        $seasons[3] = "2017-12-05";
        $seasons[4] = "2017-12-05";
        $seasons[5] = "2017-12-05";
        $seasons[6] = "2017-12-05";
        $seasons[7] = "2017-12-06";
        $seasons[8] = "2018-03-06";
        $seasons[9] = "2018-09-04";
        $seasons[10] = "2018-12-04";
        $seasons[11] = "2019-03-06";
        $seasons[12] = "2019-07-11";
        $seasons[13] = "2019-09-09";
        $seasons[14] = "2019-12-03";

        $playerObj = Player::where('username', $player)->get()->first();
        $seasonEnd = self::whereDate('created_at', '=', date($seasons[$season]))->where('player_id', $playerObj->id)->get()->first();

        return $seasonEnd;
    }

}
