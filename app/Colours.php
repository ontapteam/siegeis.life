<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colours extends Model
{
    private static $colours = array(
        'primary' => 'amber lighten-1',
        'secondary' => 'grey darken-4',
        'tabs' => 'black',
        'tabText' => 'white-text',
        'headingText' => 'cyan-text text-darken-3'
    );

    public static function getPrimaryColour()
    {
        return self::$colours['primary'];
    }

    public static function getSecondaryColour()
    {
        return self::$colours['secondary'];
    }

    public static function getTabColour()
    {
        return self::$colours['tabs'];
    }

    public static function getTabTextColour()
    {
        return self::$colours['tabText'];
    }

    public static function getHeadingTextColor()
    {
        return self::$colours['headingText'];
    }

}
