<?php namespace App\Services;

use Cache;
use Carbon\Carbon;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mockery\Exception;

/**
 * @property  urls
 */
class XBOX
{
    /**
     * XBOX constructor.
     * @param $gamertag
     */
    public function __construct($gamertag)
    {
        define('XboxAPI_URL', 'https://xboxapi.com');
        $this->gamertag = $gamertag;
        $this->api = new HttpClient;
        $this->videos = array();
        $this->xuid = $this->getXuid();
    }

    /**
     * @return mixed
     */
    public function getXuid()
    {
        // \Cache::flush();

        $cachekey = 'xuid_' . rawurlencode($this->gamertag);
        if (\Cache::has($cachekey)) {
            $xuid = \Cache::get($cachekey);
        } else {
            $response = $this->api->get(XboxAPI_URL . '/v2/xuid/' . rawurlencode($this->gamertag), [
                'headers' => ['X-Auth' => "f9ea7ea6bdbb24c112eaf9cdb646fb6e4bd0afe9"],
            ]);
            $xuid = $response->getBody()->getContents();
            $expiresAt = Carbon::now()->addMinutes(60);
            \Cache::put($cachekey, $xuid, $expiresAt);
        }
        return $xuid;
    }

    /**
     * @return mixed
     * @internal param $gamertag
     */
    public function getRawVideos()
    {
        // \Cache::flush();

        $cachekey = 'videos_' . rawurlencode($this->gamertag);
        if (\Cache::has($cachekey)) {
            $raw_videos = \Cache::get($cachekey);
        } else {
            $response = $this->api->get(XboxAPI_URL . '/v2/' . $this->xuid . '/game-clips/926771636', [
                'headers' => ['X-Auth' => "f9ea7ea6bdbb24c112eaf9cdb646fb6e4bd0afe9"],
            ]);
            $raw_videos = json_decode($response->getBody()->getContents(), true);
            $expiresAt = Carbon::now()->addMinutes(60);
            \Cache::put($cachekey, $raw_videos, $expiresAt);
        }
        return $raw_videos;
    }

    /**
     * @return array
     */
    function getFormattedVideoData()
    {
        $x = 0;
        $videos = array();
        foreach ($this->getRawVideos() as $raw_video) {
            $videos[$x]['thumbnails']['small'] = $raw_video['thumbnails'][0];
            $videos[$x]['thumbnails']['large'] = $raw_video['thumbnails'][1];
            $videos[$x]['video_url'] = $raw_video['gameClipUris'][0]['uri'];
            $videos[$x]['video_date'] = Carbon::parse($raw_video['dateRecorded'])->format('D, d/m/y, H:i:s');
            if($x == 9){
                break;
            }
            $x++;
        }
        return $videos;
    }

}