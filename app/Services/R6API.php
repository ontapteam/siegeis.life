<?php namespace App\Services;

/**
 * zHICHA.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://codecanyon.net/licenses/regular
 *
 * =================================================================
 *       RAINBOW SIX SIEGE (R6) API FOR LARAVEL USAGE NOTICE
 * =================================================================
 * This Parser is designed for Laravel 5.* or higher
 * zHICHA does not guarantee correct work of this class
 * on any other Laravel edition except Laravel 5.* or higher.
 * zHICHA does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   zHICHA
 * @package    zHICHA_R6API
 * @version    1.0
 * @copyright  Copyright (c) 2017 zHICHA. (http://codecanyon.net/user/zHICHA/portfolio?ref=zHICHA)
 * @license    http://codecanyon.net/licenses/regular
 */

use Cache;
use Carbon\Carbon;

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Promise\EachPromise;
use GuzzleHttp\PromiseInterface;
use GuzzleHttp\ResponseInterface;
use Guzzle\Common\Exception\MultiTransferException;

use Mockery\Exception;

class R6API
{
    protected $apiPrefix = 'https://api.r6stats.com/api/v1/';

    // Get API status - is the API returning a 200 response - needs rewriting using a Promise
    public function getStatus()
    {
        $host = "https://api.r6stats.com/api/v1/database/weapons";
        $header_check = get_headers($host);
        $response_code = $header_check[0];
        if ($response_code == "HTTP/1.1 200 OK") {
            return "Online.";
        } else {
            return "Offline.";
        }
    }

    // Get time until next API check on the system (could do with refactoring)
    public function getTimeUntilNextApiCheck()
    {
        $minutes = date('i', strtotime("now"));
        $diff = $this->roundUpToAny($minutes) - $minutes;

        if ($diff == 0) {
            $diff = 5;
        }
        return $diff . '<span class="small">mins</span>';

    }

    // Round up function (should probably be in a UsefulFunctions class)
    public function roundUpToAny($n, $x = 5)
    {
        return (round($n) % $x === 0) ? round($n) : round(($n + $x / 2) / $x) * $x;
    }

    // Find an individual player from the API
    public function findplayer($playerName)
    {

        $platforms = array('xone', 'ps4', 'uplay');
        $found = [];

        // Search for all three platforms at the same time
        $promises = (function () use ($platforms, $playerName) {
            $client = new Client();
            yield $client->getAsync('https://api.r6stats.com/api/v1/players/' . $playerName . '/?platform=xone');
            yield $client->getAsync('https://api.r6stats.com/api/v1/players/' . $playerName . '/?platform=ps4');
            yield $client->getAsync('https://api.r6stats.com/api/v1/players/' . $playerName . '/?platform=uplay');
        })();

        $each = new EachPromise($promises, [
            'concurrency' => 15,
            'fulfilled' => function ($response) use (&$found) {
                $array = json_decode($response->getBody()->getContents(), true);
                $found[] = json_decode(json_encode($array, JSON_FORCE_OBJECT));
            },
            'rejected' => function ($response) {
                //
            }
        ]);

        $each->promise()->wait();

        return $found;

//        $platforms = array('xone', 'uplay', 'ps4');
//
//        foreach ($platforms as $platform) {
//
//            try {
//                $api = $http->get($this->apiPrefix
//                    . 'players/'
//                    . $playerName
//                    . '/?platform='
//                    . $platform);
//
//                if ($api) {
//                    $json = $api->getBody()->getContents();
//                    $data = json_decode($json, true); // :'(
//                    return $data;
//                }
//
//            } catch (\Exception $e) {
//                //dd($e);
//            }
//
//        }
//
//        return false;
    }

    // Find a group of players (work in progress)
    public function findplayers()
    {
        $client = new HttpClient;

        $promise = $client->requestAsync('GET', $this->apiPrefix
            . 'players/'
            . 'El%20Tisch'
            . '/?platform='
            . 'xone');

        $promise->then(function (ResponseInterface $response) {
            $profile = json_decode($response->getBody(), true);
            // Do something with the profile.
        });

        $promise->wait();
    }

    // Returns data for a particular player and plataform.
    public function getplayer($playerName, $platform = null)
    {

        if (!is_null($platform)) {

            $promises = (function () use ($playerName, $platform) {
                // Get single player
                $client = new Client();
                //$promise = $client->requestAsync('GET', 'https://api.r6stats.com/api/v1/players/' . $playerName . '/?platform=' . $platform . '');
                yield $client->getAsync(request()->url() . '/api-test/player/' . $playerName . '?appcode=test&platform=' . $platform)->then(

                    function ($response) {
                        return json_decode($response->getBody()->getContents());
                    },
                    function () use ($playerName, $platform) {
                        echo "Player failed: " . $playerName . ", " . $platform . "\n";
                    }
                );

            })();

            // Complete each promise defined above
            // TODO: write output to log file
            $each = new EachPromise($promises, [
                'concurrency' => 5,
                'fulfilled' => function () {
                    //
                },
                'rejected' => function ($reason) {

                    echo "Failure." . "\n";

                    if ($reason instanceof GuzzleHttp\Exception\ClientException) {
                        $this->ddd($reason->getResponse()->getReasonPhrase());
                    } else {
                        $this->ddd($reason->getMessage());
                    }

                    echo "\n";
                }
            ]);

            $each->promise()->wait();

            // Move to a debug Class
            function ddd($v, $asString = false)
            {
                if (!$asString) {
                    array_map(function ($x) {
                        (new \Illuminate\Support\Debug\Dumper)->dump($x);
                    }, [$v]);
                } else {
                    $r = array_map(function ($x) {
                        return (new \App\Library\Classes\Dumper)->dump($x);
                    }, [$v]);
                    return $r[0];
                }
            }

            exit;

            //$statResponse = $client->getAsync(request()->url() . '/api-test/stats/' . $player->username . '?appcode=test')->then(function ($statResponse) use ($playerResponse, $player) {
//
//
//                $json = $promise->then(
//                function ($response) {
//
//                    var_dump($response);
//
////                    $array = json_decode($response->getBody()->getContents());
////
////                    foreach ($array->players as $key => $value):
////                        $array = $array->players->$key;
////                    endforeach;
////
////                    return json_decode(json_encode($array, JSON_FORCE_OBJECT));
//                },
//                function ($reason) {
//                    return $reason;
//                }
//            )->wait();


        }

        /*
        $cachekey = 'data_' . str_replace(' ', '_', $playerName);
        $http = new HttpClient;
        try {
            $api = $http->get($this->apiPrefix
                . 'players/'
                . $playerName
                . '/?platform='
                . $platform);
            if ($api) {
                $json = $api->getBody()->getContents();
                $expiresAt = Carbon::now()->addMinutes(3);
                \Cache::put($cachekey, $json, $expiresAt);
            }
            $data = json_decode($json, true); // :'(
            return $data;
        } catch (\Exception $e) {
            //dd($e);
        }

        /*
        if ($cache === "flush") {
            \Cache::forget($cachekey);
        }
        if (\Cache::has($cachekey)) {
            $json = \Cache::get($cachekey);
        } else {
            $http = new HttpClient;
            $found = false;
            if ($platform !== null) {

                try {
                    $api = $http->get($this->apiPrefix
                        . 'players/'
                        . $playerName
                        . '/?platform='
                        . $platform);
                    if ($api) {
                        $json = $api->getBody()->getContents();
                        $expiresAt = Carbon::now()->addMinutes(3);
                        \Cache::put($cachekey, $json, $expiresAt);
                    }
                } catch (\Exception $e) {
                    // dd($e->message);
                }

            } else {

                $platforms = array('xone', 'ps4', 'uplay');

                // Check to see if the player has been search already
                $existingPlayer = \App\Players::where('username', str_replace("%20", " ", $playerName))->get()->first();

                if (count($existingPlayer) > 0) {
                    try {
                        $api = $http->get($this->apiPrefix
                            . 'players/'
                            . $playerName
                            . '/?platform='
                            . $existingPlayer->platform);
                        if ($api) {
                            $json = $api->getBody()->getContents();
                            $expiresAt = Carbon::now()->addMinutes(3);
                            \Cache::put($cachekey, $json, $expiresAt);
                            $found = true;
                        }
                    } catch (\Exception $e) {
                        $found = false;
                        dd($e);
                    }
                } else {
                    foreach ($platforms as $platform) {
                        if ($found == false) {
                            try {
                                $api = $http->get($this->apiPrefix
                                    . 'players/'
                                    . $playerName
                                    . '/?platform='
                                    . $platform);
                                if ($api) {
                                    $json = $api->getBody()->getContents();
                                    $expiresAt = Carbon::now()->addMinutes(3);
                                    \Cache::put($cachekey, $json, $expiresAt);
                                    $found = true;
                                }
                            } catch (\Exception $e) {
                                $found = false;
                            }
                        }
                    }
                }
            }

        }
        $data = json_decode($json, true); // :'(
        return $data;
        */

    }

    // Returns data of operators for a particular player and platform.
    public function getplayeroperators($playerName, $platform)
    {
        $http = new HttpClient;
        $api = $http->get(
            $this->apiPrefix
            . 'players/'
            . $playerName
            . '/operators/?platform='
            . $platform
        );

        $data = json_decode($api->getBody()->getContents(), true); // :'(
        return $data;
    }

    // Returns data for weapons.
    public function getweapons()
    {
        $http = new HttpClient;
        $api = $http->get(
            $this->apiPrefix
            . 'database/weapons'
        );

        $data = json_decode($api->getBody()->getContents(), true); // :'(
        return $data;
    }

    // Returns data for gadgets.
    public function getgadgets()
    {
        $http = new HttpClient;
        $api = $http->get(
            $this->apiPrefix
            . 'database/gadgets'
        );

        $data = json_decode($api->getBody()->getContents(), true); // :'(
        return $data;
    }

    // Returns data for operators.
    public function getoperators()
    {
        $http = new HttpClient;
        $api = $http->get(
            $this->apiPrefix
            . 'database/operators'
        );

        $data = json_decode($api->getBody()->getContents(), true); // :'(
        return $data;
    }

    // Returns operators data for player.
    public function getoperatorsrecords($platform, $playerName)
    {
        $http = new HttpClient;
        $api = $http->get(
            $this->apiPrefix
            . 'players/'
            . $playerName
            . '/operators/?platform='
            . $platform
        );
        $data = json_decode($api->getBody()->getContents(), true); // :'(
        return $data;
    }

    // Get season stats
    public function getseasonstats($playerName, $platform, $cache = null)
    {
        $cachekey = 'seasondata_' . str_replace(' ', '_', $playerName);
        if ($cache === "flush") {
            \Cache::forget($cachekey);
        }
        if (\Cache::has($cachekey)) {
            $data = \Cache::get($cachekey);
        } else {
            $http = new HttpClient;
            $api = $http->get(
                $this->apiPrefix
                . 'players/'
                . $playerName
                . '/seasons/?platform='
                . $platform
            );
            $data = json_decode($api->getBody()->getContents(), true); // :'(
            $expiresAt = Carbon::now()->addMinutes(3);
            \Cache::put($cachekey, $data, $expiresAt);
        }


        return $data;
    }

    // Returns data for operators.
    public function getmaps()
    {
        $http = new HttpClient;
        $api = $http->get(
            $this->apiPrefix
            . 'database/maps'
        );

        $data = json_decode($api->getBody()->getContents(), true); // :'(
        $data = $data['maps'];

        array_push($data,
            array(
                'id' => '13',
                'name' => 'Favela',
            ),
            array(
                'id' => '14',
                'name' => 'Bartlett U.',
            ),
            array(
                'id' => '15',
                'name' => 'Skyscraper',
            ),
            array(
                'id' => '16',
                'name' => 'Coastline',
            ),
            array(
                'id' => '17',
                'name' => 'Border',
            )
        );

        usort($data, function ($a, $b) {
            return strcmp($a["name"], $b["name"]);
        });

        return $data;
    }


}