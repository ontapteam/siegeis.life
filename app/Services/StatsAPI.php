<?php

namespace App\Services;

use App\Services\UbiAPI as UbiAPI;

/**
 * Stats API for R6 Player Stats
 *
 * @property array|mixed|object progressionJson
 * @property UbiAPI uapi
 * @property mixed region
 * @property mixed platform
 * @property mixed progression
 * @property mixed stats
 * @property array data
 * @property array final
 * @property array|mixed|object ranks
 * @property array notFound
 * @property int season
 * @property array config
 * @property array|mixed|object operators
 */

class StatsAPI
{
    public function __construct()
    {
        $this->config = array(
            "appcode" => "test",
            "ubi-email" => "tom.procter1984+smurf@gmail.com",
            "ubi-password" => "M15TzBlWP2ux",
            "default-region" => "emea", //Regions: emea - ncsa - apac
            "default-stats" => "allterrohuntcoop_hard_bestscore,allterrohuntcoop_normal_bestscore,allterrohuntcoop_realistic_bestscore,allterrohuntsolo_hard_bestscore,allterrohuntsolo_normal_bestscore,allterrohuntsolo_realistic_bestscore,casualpvp_death,casualpvp_kdratio,casualpvp_kills,casualpvp_matchlost,casualpvp_matchplayed,casualpvp_matchwlratio,casualpvp_matchwon,casualpvp_timeplayed,custompvp_timeplayed,gadgetpve_chosen,gadgetpve_gadgetdestroy,gadgetpve_kills,gadgetpve_mostused,gadgetpvp_chosen,gadgetpvp_gadgetdestroy,gadgetpvp_kills,gadgetpvp_mostused,gamemodeoperatorpvp_matchlost,gamemodeoperatorpvp_matchplayed,gamemodeoperatorpvp_matchwlratio,gamemodeoperatorpvp_matchwon,generalpve_accuracy,generalpve_barricadedeployed,generalpve_blindkills,generalpve_bulletfired,generalpve_bullethit,generalpve_dbno,generalpve_dbnoassists,generalpve_death,generalpve_distancetravelled,generalpve_gadgetdestroy,generalpve_headshot,generalpve_hostagedefense,generalpve_hostagerescue,generalpve_kdratio,generalpve_killassists,generalpve_kills,generalpve_matchlost,generalpve_matchplayed,generalpve_matchwlratio,generalpve_matchwon,generalpve_meleekills,generalpve_penetrationkills,generalpve_rappelbreach,generalpve_reinforcementdeploy,generalpve_revive,generalpve_revivedenied,generalpve_serveraggression,generalpve_serverdefender,generalpve_servershacked,generalpve_suicide,generalpve_timeplayed,generalpve_totalxp,generalpvp_accuracy,generalpvp_barricadedeployed,generalpvp_blindkills,generalpvp_bulletfired,generalpvp_bullethit,generalpvp_dbno,generalpvp_dbnoassists,generalpvp_death,generalpvp_distancetravelled,generalpvp_gadgetdestroy,generalpvp_headshot,generalpvp_hostagedefense,generalpvp_hostagerescue,generalpvp_kdratio,generalpvp_killassists,generalpvp_kills,generalpvp_matchlost,generalpvp_matchplayed,generalpvp_matchwlratio,generalpvp_matchwon,generalpvp_meleekills,generalpvp_penetrationkills,generalpvp_rappelbreach,generalpvp_reinforcementdeploy,generalpvp_revive,generalpvp_revivedenied,generalpvp_serveraggression,generalpvp_serverdefender,generalpvp_servershacked,generalpvp_suicide,generalpvp_timeplayed,generalpvp_totalxp,karma_rank,missioncoop_hard_bestscore,missioncoop_normal_bestscore,missioncoop_realistic_bestscore,missionsbyplaylistpve_bestscore,missionsolo_hard_bestscore,missionsolo_normal_bestscore,missionsolo_realistic_bestscore,normalpvp_matchlost,normalpvp_matchplayed,normalpvp_matchwlratio,normalpvp_matchwon,normalpvp_timeplayed,operatorpve_ash_bonfirekill,operatorpve_ash_bonfirewallbreached,operatorpve_bandit_batterykill,operatorpve_black_mirror_gadget_deployed,operatorpve_blackbeard_gunshieldblockdamage,operatorpve_blitz_flashedenemy,operatorpve_blitz_flashfollowupkills,operatorpve_blitz_flashshieldassist,operatorpve_buck_kill,operatorpve_capitao_lethaldartkills,operatorpve_capitao_smokedartslaunched,operatorpve_castle_kevlarbarricadedeployed,operatorpve_caveira_aikilledinstealth,operatorpve_cazador_assist_kill,operatorpve_dbno,operatorpve_death,operatorpve_doc_hostagerevive,operatorpve_doc_selfrevive,operatorpve_doc_teammaterevive,operatorpve_echo_enemy_sonicburst_affected,operatorpve_frost_beartrap_kill,operatorpve_fuze_clusterchargekill,operatorpve_glaz_sniperkill,operatorpve_glaz_sniperpenetrationkill,operatorpve_headshot,operatorpve_hibana_detonate_projectile,operatorpve_iq_gadgetspotbyef,operatorpve_jager_gadgetdestroybycatcher,operatorpve_kapkan_boobytrapdeployed,operatorpve_kapkan_boobytrapkill,operatorpve_kdratio,operatorpve_kills,operatorpve_meleekills,operatorpve_montagne_shieldblockdamage,operatorpve_mostused,operatorpve_mute_gadgetjammed,operatorpve_mute_jammerdeployed,operatorpve_pulse_heartbeatassist,operatorpve_pulse_heartbeatspot,operatorpve_rook_armorboxdeployed,operatorpve_rook_armortakenourself,operatorpve_rook_armortakenteammate,operatorpve_roundlost,operatorpve_roundplayed,operatorpve_roundwlratio,operatorpve_roundwon,operatorpve_sledge_hammerhole,operatorpve_sledge_hammerkill,operatorpve_smoke_poisongaskill,operatorpve_tachanka_turretdeployed,operatorpve_tachanka_turretkill,operatorpve_thatcher_gadgetdestroywithemp,operatorpve_thermite_chargedeployed,operatorpve_thermite_chargekill,operatorpve_thermite_reinforcementbreached,operatorpve_timeplayed,operatorpve_totalxp,operatorpve_twitch_gadgetdestroybyshockdrone,operatorpve_twitch_shockdronekill,operatorpve_valkyrie_camdeployed,operatorpvp_ash_bonfirekill,operatorpvp_ash_bonfirewallbreached,operatorpvp_bandit_batterykill,operatorpvp_black_mirror_gadget_deployed,operatorpvp_blackbeard_gunshieldblockdamage,operatorpvp_blitz_flashedenemy,operatorpvp_blitz_flashfollowupkills,operatorpvp_blitz_flashshieldassist,operatorpvp_buck_kill,operatorpvp_capitao_lethaldartkills,operatorpvp_capitao_smokedartslaunched,operatorpvp_castle_kevlarbarricadedeployed,operatorpvp_caveira_interrogations,operatorpvp_cazador_assist_kill,operatorpvp_dbno,operatorpvp_death,operatorpvp_doc_hostagerevive,operatorpvp_doc_selfrevive,operatorpvp_doc_teammaterevive,operatorpvp_echo_enemy_sonicburst_affected,operatorpvp_frost_dbno,operatorpvp_fuze_clusterchargekill,operatorpvp_glaz_sniperkill,operatorpvp_glaz_sniperpenetrationkill,operatorpvp_headshot,operatorpvp_hibana_detonate_projectile,operatorpvp_iq_gadgetspotbyef,operatorpvp_jager_gadgetdestroybycatcher,operatorpvp_kapkan_boobytrapdeployed,operatorpvp_kapkan_boobytrapkill,operatorpvp_kdratio,operatorpvp_kills,operatorpvp_meleekills,operatorpvp_montagne_shieldblockdamage,operatorpvp_mostused,operatorpvp_mute_gadgetjammed,operatorpvp_mute_jammerdeployed,operatorpvp_pulse_heartbeatassist,operatorpvp_pulse_heartbeatspot,operatorpvp_rook_armorboxdeployed,operatorpvp_rook_armortakenourself,operatorpvp_rook_armortakenteammate,operatorpvp_roundlost,operatorpvp_roundplayed,operatorpvp_roundwlratio,operatorpvp_roundwon,operatorpvp_sledge_hammerhole,operatorpvp_sledge_hammerkill,operatorpvp_smoke_poisongaskill,operatorpvp_tachanka_turretdeployed,operatorpvp_tachanka_turretkill,operatorpvp_thatcher_gadgetdestroywithemp,operatorpvp_thermite_chargedeployed,operatorpvp_thermite_chargekill,operatorpvp_thermite_reinforcementbreached,operatorpvp_timeplayed,operatorpvp_totalxp,operatorpvp_twitch_gadgetdestroybyshockdrone,operatorpvp_twitch_shockdronekill,operatorpvp_valkyrie_camdeployed,plantbombpve_bestscore,plantbombpve_matchlost,plantbombpve_matchplayed,plantbombpve_matchwlratio,plantbombpve_matchwon,plantbombpve_timeplayed,plantbombpvp_bestscore,plantbombpvp_matchlost,plantbombpvp_matchplayed,plantbombpvp_matchwlratio,plantbombpvp_matchwon,plantbombpvp_timeplayed,plantbombpvp_totalxp,progression_level,progression_renown,progression_xp,protecthostagepve_bestscore,protecthostagepve_hostagedefense,protecthostagepve_hostagerescue,protecthostagepve_matchlost,protecthostagepve_matchplayed,protecthostagepve_matchwlratio,protecthostagepve_matchwon,protecthostagepve_timeplayed,rankedpvp_death,rankedpvp_kdratio,rankedpvp_kills,rankedpvp_matchlost,rankedpvp_matchplayed,rankedpvp_matchwlratio,rankedpvp_matchwon,rankedpvp_timeplayed,rescuehostagepve_bestscore,rescuehostagepve_hostagedefense,rescuehostagepve_hostagerescue,rescuehostagepve_matchlost,rescuehostagepve_matchplayed,rescuehostagepve_matchwlratio,rescuehostagepve_matchwon,rescuehostagepve_timeplayed,rescuehostagepvp_bestscore,rescuehostagepvp_matchlost,rescuehostagepvp_matchplayed,rescuehostagepvp_matchwlratio,rescuehostagepvp_matchwon,rescuehostagepvp_totalxp,secureareapve_bestscore,secureareapve_matchlost,secureareapve_matchplayed,secureareapve_matchwlratio,secureareapve_matchwon,secureareapve_serveraggression,secureareapve_serverdefender,secureareapve_servershacked,secureareapve_timeplayed,secureareapvp_bestscore,secureareapvp_matchlost,secureareapvp_matchplayed,secureareapvp_matchwlratio,secureareapvp_matchwon,secureareapvp_totalxp,terrohuntclassicpve_bestscore,terrohuntclassicpve_matchlost,terrohuntclassicpve_matchplayed,terrohuntclassicpve_matchwlratio,terrohuntclassicpve_matchwon,terrohuntclassicpve_timeplayed,weaponpve_mostkills,weaponpve_mostused,weaponpvp_mostkills,weaponpvp_mostused,weapontypepve_accuracy,weapontypepve_bulletfired,weapontypepve_bullethit,weapontypepve_chosen,weapontypepve_dbno,weapontypepve_dbnoassists,weapontypepve_death,weapontypepve_deaths,weapontypepve_efficiency,weapontypepve_headshot,weapontypepve_headshotratio,weapontypepve_kdratio,weapontypepve_killassists,weapontypepve_kills,weapontypepve_mostkills,weapontypepve_power,weapontypepvp_accuracy,weapontypepvp_bulletfired,weapontypepvp_bullethit,weapontypepvp_chosen,weapontypepvp_dbno,weapontypepvp_dbnoassists,weapontypepvp_death,weapontypepvp_deaths,weapontypepvp_efficiency,weapontypepvp_headshot,weapontypepvp_headshotratio,weapontypepvp_kdratio,weapontypepvp_killassists,weapontypepvp_kills,weapontypepvp_mostkills,weapontypepvp_power",
            //All stats: https://gist.github.com/LaxisB/3924cfdc35562b719d1c891cdb895366
            "default-platform" => "uplay", //Platforms: uplay - xbl - psn
            "default-progression" => "true" //Enabled: true - false
        );
        $this->uapi = new UbiAPI($this->config["ubi-email"], $this->config["ubi-password"]);
        $this->region = $this->config["default-region"];
        $this->platform = $this->config["default-platform"];
        $this->stats = "weapontypepvp_killassists,casualpvp_death,casualpvp_kdratio,casualpvp_kills,casualpvp_matchlost,casualpvp_matchplayed,casualpvp_matchwlratio,casualpvp_matchwon,casualpvp_timeplayed,custompvp_timeplayed,generalpvp_accuracy,generalpvp_barricadedeployed,generalpvp_blindkills,generalpvp_bulletfired,generalpvp_bullethit,generalpvp_dbno,generalpvp_dbnoassists,generalpvp_death,generalpvp_distancetravelled,generalpvp_gadgetdestroy,generalpvp_headshot,generalpvp_hostagedefense,generalpvp_hostagerescue,generalpvp_kdratio,generalpvp_killassists,generalpvp_kills,generalpvp_matchlost,generalpvp_matchplayed,generalpvp_matchwlratio,generalpvp_matchwon,generalpvp_meleekills,generalpvp_penetrationkills,generalpvp_rappelbreach,generalpvp_reinforcementdeploy,generalpvp_revive,generalpvp_revivedenied,generalpvp_suicide,generalpvp_timeplayed,generalpvp_totalxp,rankedpvp_death,rankedpvp_kdratio,rankedpvp_kills,rankedpvp_matchlost,rankedpvp_matchplayed,rankedpvp_matchwlratio,rankedpvp_matchwon,rankedpvp_timeplayed";
        $this->season = -1;
        $this->data = array();
        $this->final = array();
        $this->notFound = [];
        $this->operators = \App\Operators::getOperators();

        $this->ranks = json_decode('{
          "0": {
            "image": "https://i.imgur.com/jNJ1BBl.png",
            "name": "Unranked"
          },
          "1": {
            "image": "https://i.imgur.com/deTjm7V.png",
            "name": "Copper Ⅳ"
          },
          "2": {
            "image": "https://i.imgur.com/zx5KbBO.png",
            "name": "Copper Ⅲ"
          },
          "3": {
            "image": "https://i.imgur.com/RTCvQDV.png",
            "name": "Copper Ⅱ"
          },
          "4": {
            "image": "https://i.imgur.com/SN55IoP.png",
            "name": "Copper Ⅰ"
          },
          "5": {
            "image": "https://i.imgur.com/DmfZeRP.png",
            "name": "Bronze Ⅳ"
          },
          "6": {
            "image": "https://i.imgur.com/QOuIDW4.png",
            "name": "Bronze Ⅲ"
          },
          "7": {
            "image": "https://i.imgur.com/ry1KwLe.png",
            "name": "Bronze Ⅱ"
          },
          "8": {
            "image": "https://i.imgur.com/64eQSbG.png",
            "name": "Bronze Ⅰ"
          },
          "9": {
            "image": "https://i.imgur.com/fOmokW9.png",
            "name": "Silver Ⅳ"
          },
          "10": {
            "image": "https://i.imgur.com/e84XmHl.png",
            "name": "Silver Ⅲ"
          },
          "11": {
            "image": "https://i.imgur.com/f68iB99.png",
            "name": "Silver Ⅱ"
          },
          "12": {
            "image": "https://i.imgur.com/iQGr0yz.png",
            "name": "Silver Ⅰ"
          },
          "13": {
            "image": "https://i.imgur.com/DelhMBP.png",
            "name": "Gold Ⅳ"
          },
          "14": {
            "image": "https://i.imgur.com/5fYa6cM.png",
            "name": "Gold Ⅲ"
          },
          "15": {
            "image": "https://i.imgur.com/7c4dBTz.png",
            "name": "Gold Ⅱ"
          },
          "16": {
            "image": "https://i.imgur.com/cOFgDW5.png",
            "name": "Gold Ⅰ"
          },
          "17": {
            "image": "https://i.imgur.com/to1cRGC.png",
            "name": "Platinum Ⅲ"
          },
          "18": {
            "image": "https://i.imgur.com/vcIEaEz.png",
            "name": "Platinum Ⅱ"
          },
          "19": {
            "image": "https://i.imgur.com/HAU5DLj.png",
            "name": "Platinum Ⅰ"
          },
          "20": {
            "image": "https://i.imgur.com/Rt6c2om.png",
            "name": "Diamond Ⅰ"
          }
        }', true);

        if (isset($_GET['season'])) {
            $this->season = $_GET['season'];
        }

        if (isset($_GET['platform'])) {
            $this->platform = $_GET['platform'];
        }

        if (isset($_GET['region'])) {
            $this->region = $_GET['region'];
        }

        $this->progression = $this->config['default-progression'];
        $this->loadProgression = $this->config["default-progression"];

        if (isset($_GET["progression"])) {
            $this->loadProgression = $_GET["progression"];
        }

        if ($this->loadProgression != "true" && $this->loadProgression != "false") {
            $this->loadProgression = $this->config["default-progression"];
        }

    }

    public function checkId()
    {
        if (isset($_GET["id"])) {
            $str = $_GET["id"];
            if (strpos($str, ',') !== false) {
                $tocheck = explode(',', $str);
            } else {
                $tocheck = array(
                    $str
                );
            }

            foreach ($tocheck as $value) {
                $this->printName($value);
            }
        }
    }

    public function checkPlayer($playerToGet)
    {
        if (isset($playerToGet)) {
            $str = $playerToGet;
            if (strpos($str, ',') !== false) {
                $tocheck = explode(',', $str);
            } else {
                $tocheck = array(
                    $str
                );
            }

            foreach ($tocheck as $value) {
                $this->printID($value);
            }
        }
    }

    public function checkData()
    {
        if (empty($this->data)) {
            $error = $this->uapi->getErrorMessage();
            if ($error === false) {
                die(json_encode(array(
                    "players" => $this->notFound
                )));
            } else {
                die(json_encode(array(
                    "players" => array(),
                    "error" => $error
                )));
            }
        }
    }

    public function getIds()
    {
        $ids = "";

        $this->checkId();
        foreach ($this->data as $value) {
            $ids = $ids . "," . $value["profile_id"];
        }

        $ids = substr($ids, 1);
        return $ids;
    }

    public function getResponse($player)
    {
        $this->checkId();
        $this->checkPlayer($player);
        $this->checkData();
    }

    /**
     * Get Player Information (a combination of Ranking and Progression)
     *
     * @param $player
     * @return mixed|string|void
     */
    public function getPlayer($player)
    {
        $this->getResponse($player);
        $idresponse = json_decode($this->uapi->getRanking($this->getIds(), $this->season, $this->region, $this->platform), true);

        if ($this->loadProgression == "true") {
            $this->progressionJson = json_decode($this->uapi->getProgression($this->getIds(), $this->platform), true);
            if (!array_key_exists("player_profiles", $this->progressionJson)) {
                die(json_encode(array(
                    "players" => $this->notFound
                )));
            }
            $this->progression = $this->progressionJson["player_profiles"];
        }

        if (!isset($idresponse)) {
            die(json_encode(array(
                "players" => $this->notFound
            )));
        }

        if (!array_key_exists("players", $idresponse)) {
            die(json_encode(array(
                "players" => $this->notFound
            )));
        }

        foreach ($idresponse["players"] as $value) {
            $id = $value["profile_id"];
            $this->final[$id] = array_merge(($this->loadProgression == "true" ? $this->getValue($id, $this->progression) : array()), $value, array(
                "nickname" => $this->data[$id]["nickname"],
                "platform" => $this->platform,
                "rankInfo" => $this->ranks[$value["rank"]]
            ));
        }

        return json_encode(array(
            "players" => array_merge($this->final, $this->notFound)
        ));

    }

    /**
     * Return Stats of Player
     *
     * @param $player
     * @return mixed
     */
    public function getStats($player)
    {
        $this->getResponse($player);
        $idresponse = json_decode($this->uapi->getStats($this->getIds(), $this->stats, $this->platform), true);
        foreach ($idresponse["results"] as $value) {
            $id = array_search($value, $idresponse["results"]);
            $this->final[$id] = array_merge($value, array("nickname" => $this->data[$id]["nickname"], "profile_id" => $id, "platform" => $this->platform));
        }
        return str_replace(":infinite", "", json_encode(array("players" => array_merge($this->final, $this->notFound))));
    }

    /**
     * Return a small player object
     *
     * @param $player
     */
    public function getSmallPlayer($player)
    {
        $this->getResponse($player);
        print json_encode(array_merge($this->data, $this->notFound));
    }

    public function getOperators($player)
    {
        $this->getResponse($player);
        $idresponse = json_decode($this->uapi->getOperators($this->getIds(), $this->platform), true);

        foreach($idresponse as $id=>$value) {
            $this->final[$id] = array_merge($value, array("profile_id"=>$id, "nickname"=>$this->data[$id]["nickname"], "platform" => $this->platform));
        }

        $operatorArray = array();
        $operatorOrg = json_decode('{"zofia":{"name":"Zofia","organisation":"GROM"},"castle":{"name":"Castle","organisation":"FBI SWAT"},"jager":{"name":"Jäger","organisation":"GSG 9"},"vigil":{"name":"Vigil","organisation":"SMB"},"sledge":{"name":"Sledge","organisation":"SAS"},"echo":{"name":"Echo","organisation":"SAT"},"fuze":{"name":"Fuze","organisation":"Spetnaz"},"thermite":{"name":"Thermite","organisation":"FBI SWAT"},"blackbeard":{"name":"Blackbeard","organisation":"Navy Seal"},"buck":{"name":"Buck","organisation":"JTF2"},"frost":{"name":"Frost","organisation":"JTF2"},"caveira":{"name":"Caveira","organisation":"Bope"},"ela":{"name":"Ela","organisation":"GROM"},"capitao":{"name":"Capitão","organisation":"BOPE"},"hibana":{"name":"Hibana","organisation":"SAT"},"thatcher":{"name":"Thatcher","organisation":"SAS"},"kapkan":{"name":"Kapkan","organisation":"Spetnaz"},"twitch":{"name":"Twitch","organisation":"GIGN"},"bandit":{"name":"Bandit","organisation":"GSG 9"},"dokkaebi":{"name":"Dokkaebi","organisation":"SMB"},"smoke":{"name":"Smoke","organisation":"SAS"},"iq":{"name":"IQ","organisation":"GSG 9"},"mute":{"name":"Mute","organisation":"SAS"},"alibi":{"name":"Alibi","organisation":"GIS"},"rook":{"name":"Rook","organisation":"GIGN"},"jackal":{"name":"Jackal","organisation":"GEO"},"lion":{"name":"Lion","organisation":"CBRN"},"glaz":{"name":"Glaz","organisation":"Spetnaz"},"finka":{"name":"Finka","organisation":"CBRN"},"valkyrie":{"name":"Valkyrie","organisation":"Navy Seal"},"ying":{"name":"Ying","organisation":"SDU"},"blitz":{"name":"Blitz","organisation":"GSG 9"},"ash":{"name":"Ash","organisation":"FBI SWAT"},"mira":{"name":"Mira","organisation":"GEO"},"pulse":{"name":"Pulse","organisation":"FBI SWAT"},"doc":{"name":"Doc","organisation":"GIGN"},"montagne":{"name":"Montagne","organisation":"GIGN"},"maestro":{"name":"Maestro","organisation":"GIS"},"lesion":{"name":"Lesion","organisation":"SDU"},"maverick":{"name":"Maverick","organisation":"GSUTR"},"clash":{"name":"Clash","organisation":"GSUTR"},"nomad":{"name":"Nomad","organisation":"GIGR"},"kaid":{"name":"Kaid","organisation":"GIGR"}}', true);

        foreach($this->operators as $operator=>$info) {
            $operatorArray[$operator] = array();
            $operatorArray[$operator]["images"] = $info["images"];
            $operatorArray[$operator]["category"] = $info["category"];
            $operatorArray[$operator]["index"] = $info["index"];
            $operatorArray[$operator]["id"] = $operator;
            if(isset($operatorOrg[$operator])) {
                $info = $operatorOrg[$operator];
                $operatorArray[$operator]["organisation"] = $info["organisation"];
                $operatorArray[$operator]["name"] = $info["name"];
            }
        }

        print json_encode(array_merge(array("players" => array_merge($this->final,$this->notFound)),array("operators" => $operatorArray)));
    }

    /**
     * @param $uid
     */
    public function printName($uid)
    {
        $su = $this->uapi->searchUser("byid", $uid, $this->platform);
        if ($su["error"] != true) {
            $this->data[$su['uid']] = array(
                "profile_id" => $su['uid'],
                "nickname" => $su['nick']
            );
        } else {
            $this->notFound[$uid] = [
                "profile_id" => $uid,
                "error" => [
                    "message" => "User not found!"
                ]
            ];
        }
    }

    /**
     * @param $name
     */
    public function printID($name)
    {
        $su = $this->uapi->searchUser("bynick", $name, $this->platform);

        if ($su["error"] != true) {
            $this->data[$su['uid']] = array(
                "profile_id" => $su['uid'],
                "nickname" => $su['nick']
            );
        } else {
            $this->notFound[$name] = [
                "nickname" => $name,
                "error" => [
                    "message" => "User not found!"
                ]
            ];
        }
    }

    /**
     * @param $user
     * @param $progression
     * @return array
     */
    public function getValue($user, $progression)
    {
        foreach ($progression as $usera) {
            if ($usera["profile_id"] == $user) {
                return $usera;
            }
        }

        return array();
    }

}