<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use GuzzleHttp\Client as Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Promise\EachPromise;
use GuzzleHttp\PromiseInterface;
use GuzzleHttp\ResponseInterface;

use Guzzle\Common\Exception\MultiTransferException;

class Player extends Model
{
    protected $fillable = ['player_id', 'platform', 'username', 'uplay_id'];

    public function updatePlayer($username, $platform)
    {
        $client = new Client;

        if ($platform == "xone") {
            $platform = "xbl";
        }

        if ($platform == "ps4") {
            $platform = "psn";
        }

        $promises = [
            'player' => $client->getAsync(request()->url() . '/api-test/player/' . $username . '?appcode=test&platform=' . $platform),
            'stats' => $client->getAsync(request()->url() . '/api-test/stats/' . $username . '?appcode=test&platform=' . $platform),
        ];

        try {
            $results = Promise\settle($promises)->wait();

            if ($results['player'] && $results['stats']) {

                $profile = json_decode($results['player']['value']->getBody()->getContents());
                $stats = json_decode($results['stats']['value']->getBody()->getContents());

                foreach ($profile->players as $key => $value):
                    $profile = $profile->players->$key;
                endforeach;

                foreach ($stats->players as $key => $value):
                    $stats = $stats->players->$key;
                endforeach;

                // Prepare the User ID and Player ID
                if ($user = User::where('name', $username)->get()->first()) {
                    $user_id = $user->id;
                } else {
                    $user_id = 0;
                }

                if ($player = Player::where('username', $username)->get()->first()) {
                    $player_id = $player->id;
                } else {
                    $player_id = 0;
                }

                $record = (new Stat)->whereDate('created_at', '=', Carbon::today()->toDateString())->where('player_id', $player_id);

                if ($record->count() == 0) {
                    $record = new Stat();
                } else {
                    $record = $record->first();
                }

                // Create a new Stat record
                if (isset($stats->rankedpvp_kills)) {

                    $record->user_id = $user_id;
                    $record->player_id = $player_id;
                    $record->kills = $stats->rankedpvp_kills;
                    $record->deaths = $stats->rankedpvp_death;
                    $record->wins = $stats->rankedpvp_matchwon;
                    $record->losses = $stats->rankedpvp_matchlost;

                    // TODO: make it so that it recognises your region
                    $record->points = $profile->mmr;
                    $record->rank = $profile->rank;

                    // Save the stats record to the database
                    $record->save();

                    echo "Player updated: " . $username . ", " . $platform . "\n";
                } else {
                    echo "Player not updated: " . $username . ", " . $platform . "\n";
                    $player->delete();
                }

            }

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Update all the players in the Players table
     */
    public
    function updateAllPlayers()
    {
        $players = self::all();
        //$players = self::take(2)->get();
        foreach ($players as $player) {

            //$this->updatePlayer("dosboi", "uplay");
            $this->updatePlayer($player->username, $player->platform);
        }
    }

    /**
     * Update all of the players in the database with the latest stats
     */
    public
    function dailyStats()
    {
        $players = self::all();
        //$players = self::take(2)->get();

        $promises = (function () use ($players) {
            $client = new Client;
            foreach ($players as $player) {

                //yield $client->getAsync('https://api.r6stats.com/api/v1/players/' . $player->username . '/?platform=' . $player->platform . '')->then(
                yield $client->getAsync(request()->url() . '/api-test/player/' . $player->username . '?appcode=test')->then(

                    function ($playerResponse) use ($client, $player) {

                        //$seasonPromise = $client->getAsync('https://api.r6stats.com/api/v1/players/' . $player->username . '/seasons/?platform=' . $player->platform . '')->then(function ($seasonResponse) use ($playerResponse, $player) {
                        $statResponse = $client->getAsync(request()->url() . '/api-test/stats/' . $player->username . '?appcode=test')->then(function ($statResponse) use ($playerResponse, $player) {

                            $profile = json_decode($playerResponse->getBody()->getContents());
                            $stats = json_decode($statResponse->getBody()->getContents());

                            // Move seasons object to the end and get latest season
//                            end($seasons->seasons);
//                            $latestSeason = current($seasons->seasons);

                            // Prepare the User ID and Player ID
                            foreach ($profile->players as $key => $value):
                                $profile = $profile->players->$key;
                            endforeach;

                            foreach ($stats->players as $key => $value):
                                $stats = $stats->players->$key;
                            endforeach;

                            // Prepare the User ID and Player ID
                            if ($user = User::where('name', $player->username)->get()->first()) {
                                $user_id = $user->id;
                            } else {
                                $user_id = 0;
                            }

                            if ($player = Player::where('username', $player->username)->get()->first()) {
                                $player_id = $player->id;
                            } else {
                                $player_id = 0;
                            }

                            // Create a new Stat record
                            $record = new Stat();
                            $record->user_id = $user_id;
                            $record->player_id = $player_id;
                            $record->kills = $stats->rankedpvp_kills;
                            $record->deaths = $stats->rankedpvp_death;
                            $record->wins = $stats->rankedpvp_matchwon;
                            $record->losses = $stats->rankedpvp_matchlost;

                            // Save the stats record to the database
                            $record->save();

                            echo "Player updated: " . $player->username . ", " . $player->platform . "\n";

                        });

                        return $statResponse;
                    },
                    function () use ($player) {
                        echo "Player failed: " . $player->username . ", " . $player->platform . "\n";
                    });

            }
        })();

        // Complete each promise defined above
        // TODO: write output to log file
        $each = new EachPromise($promises, [
            'concurrency' => 5,
            'fulfilled' => function () {
                //
            },
            'rejected' => function ($reason) {

                echo "Failure." . "\n";

                if ($reason instanceof GuzzleHttp\Exception\ClientException) {
                    $this->ddd($reason->getResponse()->getReasonPhrase());
                } else {
                    $this->ddd($reason->getMessage());
                }

                echo "\n";
            }
        ]);

        $each->promise()->wait();
    }

    // Move to a debug Class
    function ddd($v, $asString = false)
    {
        if (!$asString) {
            array_map(function ($x) {
                (new \Illuminate\Support\Debug\Dumper)->dump($x);
            }, [$v]);
        } else {
            $r = array_map(function ($x) {
                return (new \App\Library\Classes\Dumper)->dump($x);
            }, [$v]);
            return $r[0];
        }
    }

}
