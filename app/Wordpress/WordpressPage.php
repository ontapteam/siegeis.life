<?php

namespace App\Wordpress;

class WordpressPage extends \Corcel\Page
{
    protected $connection = 'wordpress';

    /**
     * @return mixed
     */
    public function getPage($slug)
    {
        $page = self::slug($slug)->first();
        return $page;
    }
    
}
