<?php

namespace App\Wordpress;

use Corcel\TermTaxonomy;
use Corcel\Page;
use Corcel\Post as Corcel;
use Mockery\Exception;

class WordpressPost extends Corcel
{
    protected $connection = 'wordpress';

    /**
     * @param $content
     * @return string
     */
    public static function getExcerpt($content)
    {
        $string = strip_tags($content);

        if (strlen($string) > 300) {
            $stringCut = substr($string, 0, 300);
            $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
        }
        return $string;
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        $categories = array();
        $cats = TermTaxonomy::where('taxonomy', 'category')->get();
        foreach ($cats as $cat) {
            if ($cat->name !== "Uncategorized" && $cat->name !== "News")
                $categories[] = $cat->name;
        }

        return $categories;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function getPost($slug)
    {
        $post = WordpressPost::status('publish')->slug($slug)->first();
        return $post;
    }

    /**
     * @param null $category
     * @return mixed
     */
    public function getNews($category = null)
    {
        if ($category) {
            $posts = WordpressPost::taxonomy('category', $category)->status('publish')->orderBy('post_date', 'desc')->paginate(4);
        } else {
            $posts = WordpressPost::taxonomy('category', 'news')->status('publish')->orderBy('post_date', 'desc')->paginate(4);
        }

        return $posts;
    }

    /**
     * @return mixed
     */
    public function getFeaturedPost()
    {
        $post = WordpressPost::taxonomy('category', 'featured')->first();
        return $post;
    }

    /**
     * @return mixed
     */
    public function getWelcomePost()
    {
        try {
            $this->getConnection()->getPdo();
            $post = Page::slug('welcome')->first();
            return $post->content;

        } catch (\Exception $e) {
            return "We're struggling grabbing the intro content, but welcome to Siege is Life, and we'll be back up and running soon!";
            //die("Could not connect to the database.  Please check your configuration.");
        }

    }

}
