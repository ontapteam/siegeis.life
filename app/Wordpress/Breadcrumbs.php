<?php

namespace App\Wordpress;

use Request;
use Illuminate\Database\Eloquent\Model;

class Breadcrumbs extends Model
{

    /**
     * @return string
     */
    public function getBreadcrumbs()
    {
        $breadcrumbs = "<ul class=\"breadcrumbs\">";
        if (Request::segment(1)) {
            $breadcrumbs .= '<li><a href="/' . Request::segment(1) . '">' . str_replace('-', ' ', Request::segment(1)) . '</a></li>';
        }
        if (Request::segment(2)) {
            if (Request::segment(2) !== "category") {
                $breadcrumbs .= '<li> > <a href="/' . Request::segment(1) . '/' . Request::segment(2) . '">' . str_replace('-', ' ', Request::segment(2)) . '</a></li>';
            }
        }
        if (Request::segment(3)) {
            $breadcrumbs .= '<li> > <a href="/' . Request::segment(1) . '/' . Request::segment(2) . '/' . Request::segment(3) . '">' . str_replace('-', ' ', Request::segment(3)) . '</a></li>';
        }

        $breadcrumbs .= "</ul>";
        return $breadcrumbs;
    }
}
