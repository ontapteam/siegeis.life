<?php

namespace App;

use DB;

use Illuminate\Database\Eloquent\Model;

class Updater extends Model
{

    /**
     * Update the stats of all players
     */
    public function updatePlayers()
    {
        $player = new Player();
        $player->dailyStats();
    }

    /**
     * @return mixed
     *
     * @deprecated
     *
     */
    public function getPlayersList()
    {
        $players = Players::all();
        return $players;
    }

    /**
     * @return mixed
     *
     * @deprecated
     *
     */
    public function getUserList()
    {
        $users = User::all();
        return $users;
    }

    /**
     * Creates players from the users table if they don't exist already
     *
     * @deprecated
     *
     */
    public function updatePlayersFromUsers()
    {
        $users = User::all();
        foreach ($users as $user) {
            $player = Players::where('username', $user->name)->get()->first();
            if ($player) {
                // player exists, do nothing
            } else {
                $newPlayer = new Players();
                $newPlayer->username = $user->name;
                $newPlayer->save();
            }

        }
    }

    /**
     * Update any stats that have user_ids but the player_id is null
     *
     * @deprecated
     *
     */
    public function updateNullPlayerIdStats()
    {
        $stats = Stats::all();
        foreach ($stats as $stat) {

            $user = User::where('id', $stat->user_id)->get()->first();
            if ($user) {
                $stat->user_id = $user->id;
            } else {
                $stat->user_id = 0;
            }

            $player = Players::where('id', $stat->id)->get()->first();

            if ($player) {
                $stat->player_id = $player->id;
            } else {
                $stat->player_id = null;
            }

            $stat->save();
        }
    }

    /**
     * Pull the latest daily stats for each user
     *
     * @deprecated
     *
     */
    public function updateDailyStatsFromUsers()
    {
        $users = $this->getUserList();
        foreach ($users as $user) {

            $stat = new \App\Stats;
            $stat->user_id = $user->id;

            $player = new Players($user->name, "xone");
            $api = new Api("xone", $player->username, true);
            $stats = $api->getRankedStats();

            if ($stats->wins > $stat->wins || $stats->losses > $stat->losses) {

                $stat->kills = $stats->kills;
                $stat->deaths = $stats->deaths;
                $stat->wins = $stats->wins;
                $stat->losses = $stats->losses;
                $stat->points = $api->getMatchmaking();
                $stat->rank = $api->getRankNumber();
                $stat->save();

            }

        }

    }

    /**
     * Pull the daily stats for each player
     *
     * @deprecated
     *
     */
    public function updateDailyStatsFromPlayers()
    {
        // Load Players and create Player Obj
        $players = $this->getPlayersList();
        $p = new Players();

        foreach ($players as $player) {

            // Load api
            $api = new Api(null, $player->username, true);

            $stat = new Stats();

            $stat->player_id = $player->id;
            $stat->kills = $api->getOverallKills();
            $stat->deaths = $api->getOverallDeaths();
            $stat->wins = $api->getOverallWins();
            $stat->losses = $api->getOverallLosses();
            $stat->points = $api->getMatchmaking();
            $stat->rank = $api->getRankNumber();

            // Check for matching user
            $user = User::where('name', $player->username)->get()->first();
            if ($user) {
                $stat->user_id = $user->id;
            } else {
                $stat->user_id = 0;
            }

            $stat->save();

        }

    }


}