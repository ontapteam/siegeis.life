<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utils extends Model
{
    /**
     * Debug function
     *
     * @param $v
     * @param bool $asString
     * @return mixed
     */
    function ddd($v, $asString = false)
    {
        if (!$asString) {
            array_map(function ($x) {
                (new \Illuminate\Support\Debug\Dumper)->dump($x);
            }, [$v]);
        } else {
            $r = array_map(function ($x) {
                return (new \App\Library\Classes\Dumper)->dump($x);
            }, [$v]);
            return $r[0];
        }
    }


    /**
     * Debug the player output and give a load time for the page
     * @param $_player
     */
    public function debug($_player)
    {
        dd($_player, (microtime(true) - LARAVEL_START));
    }
}
