<?php

namespace App\Console\Commands;

use App\Player;
use Illuminate\Console\Command;

class UpdatePlayers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:players';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all of the players';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $player = new Player();
        $player->updateAllPlayers();
        $this->info('complete');
    }
}
