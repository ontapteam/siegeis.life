<?php

namespace App\Console\Commands;

use App\Player;
use Illuminate\Console\Command;

class UpdatePlayer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:player {player} {platform}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the selected Player';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $player = new Player();
        //$player->dailyStats();
        $player->updatePlayer($this->argument('player'), $this->argument('platform'));
        //$player->updateAllPlayers();
        $this->info('complete');
    }
}
