<?php

namespace App\Console;

use \Cache;
use App\Updater;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\UpdatePlayer::class,
        \App\Console\Commands\UpdatePlayers::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            $cron = new Updater;
            $cron->updatePlayers();
            \Cache::flush();

        })->daily();

//        $schedule->call(function () {
//
//            $cron = new Updater;
//            $cron->updatePlayers();
//            \Cache::flush();
//
//        })->everyFiveMinutes();

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
