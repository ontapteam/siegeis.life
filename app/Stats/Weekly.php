<?php

namespace App\Stats;

use Illuminate\Database\Eloquent\Model;

class Weekly extends Model
{

    /**
     * @param $current_week
     * @param null $previous_week
     * @return \stdClass
     */
    public function getWeekly($current_week, $previous_week = null)
    {
        $i = 0;
        $weekly = new \stdClass();
        $previous_day = new \stdClass();
        $weekly->kills = 0;
        $weekly->deaths = 0;
        $weekly->wins = 0;
        $weekly->losses = 0;
        $weekly->points = 0;

        if (is_null($previous_week)) {
            array_reverse($current_week);
            foreach ($current_week as $day) {
                if ($i > 0) {
                    $previous_day = $current_week[$i - 1];
                    $weekly->kills = $weekly->kills + ($previous_day->kills - $day->kills);
                    $weekly->deaths = $weekly->deaths + ($previous_day->deaths - $day->deaths);
                    $weekly->wins = $weekly->wins + ($previous_day->wins - $day->wins);
                    $weekly->losses = $weekly->losses + ($previous_day->losses - $day->losses);
                    $weekly->points = $current_week[0]->points - $current_week[count($current_week) - 1]->points;
                } elseif ($i == count($current_week)) {
                    break;
                }
                $i++;
            }
        } else {
            $weekly->kills = $current_week[0]->kills - $previous_week[0]->kills;
            $weekly->deaths = $current_week[0]->deaths - $previous_week[0]->deaths;
            $weekly->wins = $current_week[0]->wins - $previous_week[0]->wins;
            $weekly->losses = $current_week[0]->losses - $previous_week[0]->losses;
            $weekly->points = $current_week[0]->points - $previous_week[0]->points;
        }

        $weekly->deaths = "-" . $weekly->deaths;
        $weekly->losses = "-" . $weekly->losses;

        return $weekly;
    }

}
