<?php

namespace App\Stats;

use Illuminate\Database\Eloquent\Model;

class Calc extends Model
{

    /**
     * Get the difference between yesterday's stats and today's
     * @param $key
     * @param $stats
     * @param $i
     * @param $daily
     * @param string $direction
     * @return int|string
     */
    public function getDiff($key, $stats, $i, $daily, $direction = "desc")
    {
        $diff = 0;
        if ($direction == "desc") {
            if ($i + 1 == count($stats)) {
                $diff = 0;
            } else {
                $diff = $stats[$i][$key] - $stats[$i + 1][$key];
            }
        } elseif ($direction == "asc") {
            if ($i == 0) {
                $diff = 0;
            } else {
                $diff = $stats[$i][$key] - $stats[$i - 1][$key];
            }
        }

        if ($key == "deaths" || $key == "losses") {
            $diff = "-" . $diff;
        }

        return $diff;
    }

    /**
     * Get difference of scores between today and yesterday
     * @param $key
     * @param null $previous_day
     * @param null $current_day
     * @param null $i
     * @return int|string
     */
    public function getDifference($key, $current_day, $previous_day)
    {
        $diff = 0;
        if (isset($previous_day->kills)) {
            $diff = $current_day->$key - $previous_day->$key;
        }

        if ($key == "deaths" || $key == "losses") {
            $diff = "-" . $diff;
        }

        return $diff;
    }
}
