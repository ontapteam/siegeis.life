<?php

namespace App\Stats;

use Illuminate\Database\Eloquent\Model;

class Daily extends Model
{

    /**
     * @param $previous_day
     * @param $current_day
     * @return mixed
     * @internal param $i
     * @internal param $daily
     */
    public function calcStats($previous_day, $current_day)
    {
        $daily = new \stdClass();

        $daily->daily_kills = $this->getDifference("kills", $current_day, $previous_day);
        $daily->daily_deaths = $this->getDifference("deaths", $current_day, $previous_day);
        $daily->daily_wins = $this->getDifference("wins", $current_day, $previous_day);
        $daily->daily_losses = $this->getDifference("losses", $current_day, $previous_day);
        $daily->daily_points = $this->getDifference("points", $current_day, $previous_day);

        return $daily;
    }

}
