<?php

namespace App\Stats;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{
    /**
     * Get available ranks in Rainbow Six Siege
     * @return array
     */
    public static function getAvailableRanks()
    {
        $availableRanks = array(
            0 => array('Unranked', 0),
            1 => array('Copper V', 1100),
            2 => array('Copper IV', 1200),
            3 => array('Copper III', 1300),
            4 => array('Copper II',1400),
            5 => array('Copper I', 1500),
            6 => array('Bronze V', 1600),
            7 => array('Bronze IV', 1700),
            8 => array('Bronze III', 1800),
            9 => array('Bronze II', 1900),
            10 => array('Bronze I', 2000),
            11 => array('Silver V', 2100),
            12 => array('Silver IV', 2200),
            13 => array('Silver III', 2300),
            14 => array('Silver II', 2400),
            15 => array('Silver I', 2500),
            16 => array('Gold III', 2600),
            17 => array('Gold II', 2800),
            18 => array('Gold I', 3000),
            19 => array('Platinum III', 3200),
            20 => array('Platinum II', 3600),
            21 => array('Platinum I', 4000),
            22 => array('Diamond', 4400),
            23 => array('Champion', 5000)
        );
        return $availableRanks;
    }

    /**
     * Get player's current rank
     * @param null $rankNumber
     * @return mixed
     */
    public function getRank($rankNumber = null)
    {
        $availableRanks = $this->getAvailableRanks();

        if (!isset($rankNumber)) {
            $rankNumber = 0;
        }

        if ($rankNumber > 0) {
            return $availableRanks[$rankNumber];
        } else {
            return "Not ranked yet";
        }

    }

    /**
     * Return the value for each rank to use in rank slider
     * @return array
     */
    public function getRankScore($rank, $before = false)
    {
        $availableRanks = $this->getAvailableRanks();

        $i = 1;
        foreach ($availableRanks as $rankArray) {
            if ($rank == $rankArray[0] && $before == true) {
                if (($i + 1) <= 20) {
                    return $this->getRankScore($availableRanks[$i + 1][0]);
                }
            }
            if ($rank == $rankArray[0]) {
                return $rankArray[1];
            }
            $i++;
        }

        return $availableRanks;
    }

    /**
     * Calculate the distance between ranks from current score
     * @return array
     */
    public function getRankDifference($rank, $matchmaking)
    {
        $rankScore = $this->getRankScore($rank);

        if ($rankScore < $matchmaking) {
            $rankDifference = $matchmaking - $this->getRankScore($rank, true);
        } else {
            $rankDifference = $this->getRankScore($rank) - $matchmaking;
        }

        return $rankDifference;
    }

    /**
     * Return a CSS friendly name for the rank
     * @return array
     */
    public function getRankClass($rank)
    {
        return str_replace(" ", "_", $rank);
    }

    /**
     * Get number of points required to rank up
     *
     * @return float
     */
    public function getRankUpDistance()
    {
        return round($this->rankup);
    }

    /**
     * Get number of points required to rank down
     *
     * @return float
     */
    public function getRankDownDistance()
    {
        return round($this->rankdown);
    }

    /**
     * Get raw Rank number
     * @param null $season
     * @param null $region
     * @return int
     */
    public function getRankNumber($season = null, $region = null)
    {
        if ($season == null && $region == null) {
            $rankNumber = (int)$this->rank_details['rank'];
        } else {
            $rankNumber = (int)$this->seasons[$season][$region]['ranking']['rank'];
        }
        return $rankNumber;
    }

    /**
     * Get Rank Name
     * @param null $season
     * @param null $region
     * @return mixed
     */
    public function getRankName($season = null, $region = null)
    {
        return $this->getRank($this->getRankNumber($season, $region));
    }

    /**
     * Get the highest rank for the player (not working)
     * @param null $season
     * @param null $region
     * @return int
     */
    public function getHighestRank($season = null, $region = null)
    {
        if ($season == null && $region == null) {
            return (int)$this->rank_details['rank'];
        } else {
            return (int)$this->seasons[$season][$region]['ranking']['rank'];
        }
    }

    /**
     * Get Matchmaking Value (aka Players current rank score)
     * @return int
     */
    public function getMatchMaking($season = null, $region = null)
    {

        if ($season == null && $region == null) {
            return round($this->rank_details['rating']);
        } else {
            $matchingMaking = (float)$this->seasons[$season][$region]['ranking']['rating'];
            return round($matchingMaking);
        }

    }

}
