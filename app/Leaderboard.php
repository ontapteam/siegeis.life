<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leaderboard extends Model
{
    /**
     * @return mixed
     */
    public static function getTop10Wins()
    {
        $top10 = Player::orderBy('wins', 'desc')->limit(10)->get();
        return $top10;
    }
}
